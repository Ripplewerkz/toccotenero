<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/base/jquery-ui.css" type="text/css" />

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.js"></script>

<link href="{$base_dir}modules/internetbanking/css/update.css" rel="stylesheet" type="text/css">

<script src="{$base_dir}modules/internetbanking/js/update.js"></script>





{if $submit == true}

<h1 class="bcrumb">Payment Details Update</h1>



{assign var='current_step' value='payment'}



<h1>Thank you for your updating you payment details</h1>



<p id="bank-update-submit"> We will verify your payment within 24 hours (excluding weekends and public holidays). </p>

<p> Please check your spam/junk inbox in case our email landed there without your notice. Do add the email address <a href="mailto:{$shop_email}">{$shop_email}</a> to your contacts so that our emails can be sent successfully to you. Thank you! :)</p>



{else}



{if $status_message != ''}

<div id="status_message" class="warning">

{$status_message}

</div>

{/if}



<p style="padding-bottom:0; text-transform: uppercase;"><strong>The order total is: <span class="price">{$total_to_pay}</span></strong></p>

<br class="clear" />

<p>Your order has been confirmed. Please transfer the total amount using the details below:</p>

{if $banks}<br class="clear" />



    {foreach from=$banks item=bank_item}

    <table id="bank-show-{$bank_item.bank}" class="bank-show" style="float: left; margin-right: 20px;">

        <tr>

            <td><img src="{$this_path}/bank-logos/{$bank_item.logo}" title="{$bank_item.bank}" alt="{$bank_item.bank}"></td>

            <td>{$bank_item.bank}</td>

        </tr>

        <tr>

            <td class="first_item">Account number</td>

            <td class="second_item">{$bank_item.number}</td>

        </tr>

        <tr>

            <td class="first_item">Account type</td>

            <td class="second_item">{$bank_item.type}</td>

        </tr>

        <tr>

            <td class="first_item">Additional details</td>

            <td class="second_item">{$bank_item.additional}</td>

        </tr>

    </table>

    {/foreach}



<br class="clear" />

{/if}

<hr class="clear" style="color:#fff; border:0; border-bottom:1px #ccc dashed; margin:10px 0 0;" /><br />

<h2 class="bcrumb">After you have made the transfer, please enter the details of your transaction.</h2><br />



<form action="{$link->getModuleLink('internetbanking', 'update', [], true)}" method="post" id="my_banking_form" enctype="multipart/form-data">

<p>



<input type="hidden" id="order_id" name="order_id" value="{$id_order}" />

<input type="hidden" id="customer_id" name="customer_id" value="{$cookie->id_customer}" />



<label for="bank">Bank account paid to:</label>

<select id="bank" name="bank">

	<option value="#noselect#" selected="selected">Please select a bank &nbsp; </option>

        {foreach from=$banks item=bank_item}

        <option value="{$bank_item.bank}">{$bank_item.bank}</option>

        {/foreach}

</select>

</p>

<p>

<label for="bank">Mode of payment:</label>

<select id="payment_mode" name="payment_mode">

	<option value="#noselect#" selected="selected">Please select mode of payment</option>

        <option value="iBanking">Internet Banking</option>

        <option value="ATM Transfer">ATM Transfer </option>

        <option value="Interbank Transfer">Interbank Transfer</option>

</select>

</p>



<p>

<label for="trans-id">Transaction reference</label>

<input type="text" id="trans-id" name="trans-id">

</p>



<div id="nick_container">

<p>

<label for="trans-nick">Nickname</label>

<input type="text" id="trans-nick" name="trans-nick"><br class="clear" />

</p>

</div>





<div id="upload_container">

<p>

<label for="trans-upload">Attach ATM receipt</label>

<input type="file" id="trans-upload" name="trans-upload"><br class="clear" />

</p>

</div>



<p>

<label for="trans-date">Transaction date</label>

<input type="text" id="trans-date" class="mydatepricker" name="trans-date"><br class="clear" />

</p>

<p>

<label for="trans-time">Transaction time</label>

<input type="text" id="trans-time" name="trans-time"><br class="clear" />

</p>

<input type="submit" name="submit" value="Update status" class="btn" id="update_status">

</form>



{/if}

<br class="clear" />

<p><strong>Please note:</strong><br />

If you have made payment via Interbank transfer, your payment will take three business days to be verified.</p>


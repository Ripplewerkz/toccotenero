{capture name=path}{l s='Payment'}{/capture}
<h1 class="page-heading">My Payment</h1>
<br />
<br />
<div class="ibanking-wrap">
<h2 class="bcrumb">You have chosen to pay by iBanking / ATM Transfer</h2>

<div class="ibanking-contents">
<p>Your total order amount is: <span class="price"><strong>{convertPrice price=$total}</strong></span></p> 


<hr />
<form action="{$link->getModuleLink('internetbanking', 'validation', [], true)}" method="post">

<p>We accept bank transfers to the following banks:</p>
{foreach from=$bank item=banks_item}

<img src="{$this_path}bank-logos/{$banks_item.logo}" alt="{$banks_item.bank}" title="{$banks_item.bank}" style="vertical-align: top; margin-right: 15px; margin-top: 10px; margin-bottom: 20px;">

{/foreach}

<p>You may amend your order by clicking on <strong>Shopping Bag</strong> at the top right corner of the page. Otherwise, please click the Confirm button to proceed with your payment. <br />
Please note that payment has to be made within 12 hours.</p>

<div class="ch-co-bottom pt3 clearfix">

        <a href="javascript:history.go(-1)" class="ch-bo-btn bp-set white fl"><i class="fa fa-chevron-left"></i>{l s='Go Back' mod='ibanking'}</a>

        <button name="submit" class="ch-bo-btn bp-set fr">{l s='Confirm order' mod='cheque'}<i class="fa fa-chevron-right"></i></button>

</div>

</form>
</div>
</div>
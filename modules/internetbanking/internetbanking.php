<?php
/*
* 2013 Ripplewerkz Pte. Ltd.
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@Ripplewerkz.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade Ripplewerkz to newer
* versions in the future. If you wish to customize Ripplewerkz for your
* needs please refer to http://www.Ripplewerkz.com for more information.
*
*  @author Ripplewerkz SA <sales@ripplewerkz.com>
*  @copyright  2013 Ripplewerkz Pte. Ltd.
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of Ripplewerkz Pte. Ltd.
*/

if (!defined('_PS_VERSION_'))
    exit;

class Internetbanking extends PaymentModule {

    private $_status_transfer_text = 'Awaiting iBanking/ATM transfer';
    private $_status_verification_text = 'Awaiting iBanking/ATM verification';

	public function __construct() {
                
		$this->name = 'internetbanking';
		$this->tab = 'payments_gateways';
		$this->version = '0.20';
		
		$this->currencies = true;
		$this->currencies_mode = 'checkbox';

		parent::__construct();

		$this->displayName = "Internet Banking";
		$this->description = "Accept bank transfers to multiple accounts. <br /> <br /> *Note: Do not forget the mail templates found in ( modules/internetbanking/mails/en/ ) to put in ( root_folder/mails/en/ )";
	}

	public function install() {
            
        $parent = parent::install();
        
        if (!$parent) {
            return false;
        }
        
        //Hooks
        $hooks[] = $this->registerHook('payment');
        $hooks[] = $this->registerHook('paymentReturn');
        
        if (in_array(false, $hooks)) {
            return false;
        }

        //Create the i_banking_url based on server settings
        Configuration::updateValue('i_banking_url', _PS_BASE_URL_.__PS_BASE_URI__.'modules/'.$this->name.'/');
        
        //Create status messages
        $this->create_statuses();
        
        /*Create 'banking_nick' column on customer table
        $query = Db::getInstance()->Execute('
            ALTER TABLE '._DB_PREFIX_.'customer ADD `banking_nick` VARCHAR(100) NOT NULL
        ');*/
        
        /*Create transfer method on order table
        $query = Db::getInstance()->Execute('
            ALTER TABLE '._DB_PREFIX_.'orders ADD `ibanking_method` VARCHAR(100) NOT NULL
        ');*/

        //Create main table for module
        return Db::getInstance()->Execute('
            CREATE TABLE '._DB_PREFIX_.'internetbanking (
                    `id` int(6) NOT NULL AUTO_INCREMENT,
                    `bank` varchar(255) NOT NULL,
                    `logo` varchar(255) NOT NULL,
                    `number` varchar(255) NOT NULL,
                    `type` varchar(255) NOT NULL,
                    `additional` varchar(255) NOT NULL,
                    PRIMARY KEY(`id`)
            ) ENGINE=MyISAM default CHARSET=utf8'
        );
        
        return true;
	}

	public function uninstall() {
                
                $parent = parent::uninstall();
                $drop = Db::getInstance()->Execute('DROP TABLE '._DB_PREFIX_.'internetbanking');
                
                if (!$parent || !$drop) {
                    return false;
                }
                
                return true;
 	}
        
    public function getContent() {
            
            $pagination='';
            //Add edit bank
            if (Tools::isSubmit('add_edit_status')) {
                    if (isset($_POST['id']) && $_POST['id'] != '') {
                        //An ID has been set, update
                        $this->bank_update($_POST['id'], $_POST['bank'], $_POST['logo'], $_POST['number'], $_POST['type'], $_POST['additional']);
                    }
                    else {
                        //No ID, insert new record
                        $this->bank_insert($_POST['bank'], $_POST['logo'], $_POST['number'], $_POST['type'], $_POST['additional']);
                    }

                    $this->refresh();
            }
            
            //Delete bank
            if (Tools::isSubmit('delete')) {

                    $this->bank_delete($_POST['id']);
                    
                    $this->refresh();
                    
	        }
            
            //Upload bank logo
            if (Tools::isSubmit('upload_button')) {
                
                    $file = $_FILES['logo_file'];
                    if (!empty($file['name'])) {
                        $this->upload($file);
                    }
                    
            }
            
            //Search by nick
            $results_nick = array();
            if (Tools::isSubmit('search_nick_button')) {

                    $results_nick = $this->search_nick($_POST['search_term'], $_POST['page']);
                    $pagination = $this->pagination_nick($_POST['search_term'], $_POST['page']);
                    
	        }
            
            //Search by method
            $results_method = array();
            if (Tools::isSubmit('search_method_button')) {

                    $results_method = $this->search_method($_POST['search_method'], $_POST['page']);
                    $pagination = $this->pagination_method($_POST['search_method'], $_POST['page']);
                    
	        }
            
            //Update URL setting
            if (Tools::isSubmit('settings')) {

                    Configuration::updateValue('i_banking_url', $_POST['url']);

                    $this->refresh();
	        }
            
            //Update status
            if (Tools::isSubmit('status_submit')) {
                    Configuration::updateValue('ibank_status_transfer', $_POST['status_transfer']);
                    Configuration::updateValue('ibank_status_verification', $_POST['status_verification']);
                    //Redirect
                    $location = $_SERVER['REQUEST_URI'];
        			header("Location: $location");
        			exit();
            }
            
            //Get transfer and verification status settings
            $data['status_transfer'] = Configuration::get('ibank_status_transfer');
            $data['status_verification'] = Configuration::get('ibank_status_verification');
            
            //Get statuses
            $data['status_list'] = $this->get_statuses();
            
            //Get bank logos
            $data['logos'] = $this->dir_list(_PS_MODULE_DIR_.$this->name.'/bank-logos');
            
            //Compile data for view
            $data['results_nick'] = $results_nick;
            $data['results_method'] = $results_method;
            $data['pagination'] = $pagination;
            $data['this_url'] = $_SERVER['REQUEST_URI'];
            $data['banks'] = $this->get_banks();
            $data['i_banking_url'] = Configuration::get('i_banking_url');

    		//Display the backend module form
    		return $this->display_view('ripplewerkz_header', $data, true) . $this->display_view('backend_view', $data, true);
    }
        
    private function display_view($view_file, $data, $return=false) {

            //Create path to file
            $ext = explode('.', $view_file);
            if (end($ext) != 'php') {
                $view_file = $view_file.'.php';
            }
            $view_file = 'views/'.$view_file;
            
            //Include vars
            extract($data);
            
            //Buffer output
            ob_start();
            
            //Include view
            include($view_file);
            
            //Return or output?
            if ($return == true) {
                $buffer = ob_get_contents();
                @ob_end_clean();
                return $buffer;
            }
            ob_end_flush();
            @ob_end_clean();
    }
        
    private function search_nick($nickname, $page=NULL, $per_page=20) {
        
            global $cookie;
            
            $page = $this->calculate_page($page, $per_page);
        
            $output = array();
            
            //Get customer ID and email
            $sql = "
            SELECT id_customer, email FROM "._DB_PREFIX_."customer WHERE banking_nick='".$nickname."'
            ";
            $customer = Db::getInstance()->ExecuteS($sql);
            
            //If there is a result, get the orders related to the customer
            if (count($customer > 0)) {
                $sql = "
                SELECT o.`id_order`, o.`date_add`, (SELECT osl.`name`
                                                    FROM "._DB_PREFIX_."order_history AS oh
                                                    JOIN "._DB_PREFIX_."order_state_lang AS osl ON (osl.`id_order_state` = oh.`id_order_state` AND osl.`id_lang` = '".intval($cookie->id_lang)."')
                                                    WHERE oh.`id_order` = o.`id_order`
                                                    ORDER BY oh.`date_add` DESC
                                                    LIMIT 1
                ) AS `status`
                FROM "._DB_PREFIX_."orders AS o
                WHERE o.`id_customer`='".$customer[0]['id_customer']."'
                ORDER BY o.`date_add` DESC
                ";
                if ($page !== NULL) {
                   $sql .= " LIMIT ".$page.",".$per_page;
                }
                
                $orders = Db::getInstance()->ExecuteS($sql);
            }

            if (count($orders) > 0) {
                
                $output = array(
                    'email' => $customer[0]['email'],
                    'orders' => $orders
                );
                
            }
            
            return $output;
    }
        
    private function search_method($method, $page=NULL, $per_page=20) {
        
            global $cookie;
            
            $page = $this->calculate_page($page, $per_page);
        
            $output = array();
            
            //Get orders where method = search method
            $sql = "
            SELECT o.`id_order`, o.`date_add`, (SELECT osl.`name`
                                                FROM "._DB_PREFIX_."order_history AS oh
                                                JOIN "._DB_PREFIX_."order_state_lang AS osl ON (osl.`id_order_state` = oh.`id_order_state` AND osl.`id_lang` = '".intval($cookie->id_lang)."')
                                                WHERE oh.`id_order` = o.`id_order`
                                                ORDER BY oh.`date_add` DESC LIMIT 1 ) AS `status`
            FROM "._DB_PREFIX_."orders AS o
            WHERE o.`ibanking_method`='".$method."'
            ORDER BY o.`date_add` DESC
            ";
            if ($page !== NULL) {
               $sql .= " LIMIT ".$page.",".$per_page;
            }
            
            $output = Db::getInstance()->ExecuteS($sql);

            return $output;
    }
        
    private function pagination_nick($nickname, $page=NULL, $per_page=20) {
        
            if ($page === NULL) { return ''; }
            
            $output = '';
            
            //Get total count
            $total = $this->search_nick($nickname);
            $total = count($total['orders']);
            $pages = ( $total / $per_page );
            
            for ($i = 1; $i <= $pages; $i++) {
                if ($i != $page) {
                    $url = "javascript:nick_page('".$i."');";
                    $output .= '<a href="'.$url.'">'.$i.'</a>';
                }
                else {
                    $output .= '<span>'.$i.'</span>';
                }
            }
            
            return $output;
    }
        
    private function pagination_method($method, $page=NULL, $per_page=20) {
            
            if ($page === NULL) { return ''; }
            
            $output = '';
            
            //Get total count
            $total = $this->search_method($method);
            $total = count($total);
            $pages = ( $total / $per_page );
            
            for ($i = 1; $i <= $pages; $i++) {
                if ($i != $page) {
                    $url = "javascript:method_page('".$i."');";
                    $output .= '<a href="'.$url.'">'.$i.'</a>';
                }
                else {
                    $output .= '<span>'.$i.'</span>';
                }
            }
            
            return $output;
    }
        
    private function calculate_page($page, $per_page) {
            if ($page === NULL) { return NULL; }
            return ($page * $per_page) - $per_page;
    }
        
    public function upload($file) {
            $ext = explode('.', $file['name']);
            $ext = end($ext);
            if ($ext == 'png' || $ext == 'gif' || $ext == 'jpg' || $ext == 'jpeg') {
                $gen = str_replace(array('.', ' '), '', microtime()).'.'.$ext;
                move_uploaded_file($file['tmp_name'], _PS_MODULE_DIR_.$this->name.'/bank-logos/'.$gen);
                $url = 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
                header('Location: '.$url);
                exit();
            }
            else {
                exit('File upload was not an image.');
            }
    }
        
    public function get_statuses() {
            $sql = "SELECT main.id_order_state, lang.name FROM "._DB_PREFIX_."order_state AS main JOIN "._DB_PREFIX_."order_state_lang AS lang ON lang.id_order_state = main.id_order_state WHERE lang.id_lang = 1";
            return Db::getInstance()->ExecuteS($sql);
    }

    public function get_banks() {
            $sql = "SELECT * FROM "._DB_PREFIX_."internetbanking ORDER BY id";
            return Db::getInstance()->ExecuteS($sql);
    }

    private function bank_update($id, $bank, $logo, $number, $type, $additional) {
            $sql = "UPDATE "._DB_PREFIX_."internetbanking
            SET bank='".$bank."',logo='".$logo."',number='".$number."',type='".$type."',additional='".$additional."'
            WHERE id='".$id."'";
            if (Db::getInstance()->Execute($sql) == false) {
                $this->database_error($sql);
            }
    }

    private function bank_insert($bank, $logo, $number, $type, $additional) {

        if ($bank != '') {

            $sql = "INSERT INTO "._DB_PREFIX_."internetbanking (bank,logo,number,type,additional) VALUES
                ('".$bank."','".$logo."','".$number."','".$type."','".$additional."')";
            if (Db::getInstance()->Execute($sql) == false) {
                $this->database_error($sql);
            }

        }            
    }

    private function bank_delete($id) {
            $sql = "DELETE FROM "._DB_PREFIX_."internetbanking WHERE id='".$id."'";
            if (Db::getInstance()->Execute($sql) == false) {
                $this->database_error($sql);
            }
    }
        
    public function append_nickname($nickname, $id_customer) {
            $sql = "UPDATE "._DB_PREFIX_."customer SET banking_nick='".addslashes($nickname)."' WHERE id_customer=".$id_customer;
            if (Db::getInstance()->Execute($sql) == false) {
                $this->database_error($sql);
            }
    }
        
    public function append_method($method_name, $id_order) {
            $sql = "UPDATE "._DB_PREFIX_."orders SET ibanking_method='".$method_name."' WHERE id_order=".$id_order;
            if (Db::getInstance()->Execute($sql) == false) {
                $this->database_error($sql);
            }
    }
        
    private function database_error($sql='') {
            echo 'Query: <br>' . $sql . '<br><br>';
            echo 'MySQL error: <br>' . mysql_error() . '<br><br>';
            exit();
    }

    public function execPayment($cart) {
                
		if (!$this->active) {
                    return;
                }

		global $cookie, $smarty;

		$smarty->assign(array(
            'bank' => $this->get_banks(),
			'total' => number_format($cart->getOrderTotal(true, 3), 2, '.', ''),
			'this_path' => $this->_path,
            'this_path_ssl' => (Configuration::get('PS_SSL_ENABLED') ? 'https://' : 'http://').htmlspecialchars($_SERVER['HTTP_HOST'], ENT_COMPAT, 'UTF-8').__PS_BASE_URI__.'modules/'.$this->name.'/'
		));

		return $this->display(__FILE__, 'payment_execution.tpl');
	}
        
    private function create_statuses() {
        
            //Get the statuses from the config
            $transfer_status = Configuration::get('ibank_status_transfer');
            $verification_status = Configuration::get('ibank_status_verification');
            
            if (empty($transfer_status) && empty($verification_status)) {
                
                //Create the new status
                $transfer_status = $this->create_order_state($this->_status_transfer_text);
                $verification_status = $this->create_order_state($this->_status_verification_text);
                
                Configuration::updateValue('ibank_status_transfer', $transfer_status);
                Configuration::updateValue('ibank_status_verification', $verification_status);
                
            }
            
            return true;

    }
        
    private function create_order_state($label) {
            //Create the new status
            $os = new OrderState();
            $os->name = array(
                '1' => $label,
                '2' => '',
                '3' => ''
            );

            $os->invoice = false;
            $os->unremovable = true;
            $os->color = 'lightblue';

            $os->save();
            
            return $os->id;
    }
        
    private function dir_list($directory) {

            // create an array to hold directory list
            $results = array();

            // create a handler for the directory
            $handler = opendir($directory);

            // open directory and walk through the filenames
            while ($file = readdir($handler)) {

              // if file isn't this directory or its parent, add it to the results
              if ($file != "." && $file != "..") {
                $results[] = $file;
              }

            }

            // tidy up: close the handler
            closedir($handler);

            // done!
            return $results;
    }
        
    private function refresh() {
        //Redirect
        $location = $_SERVER['REQUEST_URI'];
        header("Location: $location");
        exit();
    }
        
    private function remove_get($remove) {
        
        $string = $_SERVER['REQUEST_URI'];

        $parts = parse_url($string);

        $queryParams = array();
        parse_str($parts['query'], $queryParams);

        //Now just remove the parameter

        if (isset($queryParams[$remove])) {
            unset($queryParams[$remove]);
        }

        //and rebuild the url

        $queryString = http_build_query($queryParams);
        $url = $parts['path'] . '?' . $queryString;
        
        return $url;
    }

    public function hookPayment($params) {        

        if (!$this->active)
            return;

        if (!$this->checkCurrency($params['cart']))
            return;


        $this->smarty->assign(array(
            'this_path' => $this->_path,
            'this_path_ssl' => Tools::getShopDomainSsl(true, true).__PS_BASE_URI__.'modules/'.$this->name.'/'
        ));

        return $this->display(__FILE__, 'payment.tpl');
    }

	public function hookPaymentReturn($params) {
            
        if (!$this->active)
            return;

        $state = $params['objOrder']->getCurrentState();

        if ($state == Configuration::get('ibank_status_transfer') || $state == Configuration::get('PS_OS_OUTOFSTOCK')) {

            foreach ( Module::getPaymentModules() as $module ) :

                if ($module['name'] == 'internetbanking') :

                    $ibank = new Internetbanking();             

                    $banks = $ibank->get_banks();

                endif;

            endforeach;
            
            $this->smarty->assign(array(
                'this_path'     => $this->_path,
                'banks'         => $banks,
                'total_to_pay'  => Tools::displayPrice($params['total_to_pay'], $params['currencyObj'], false),
                'status'        => 'ok',
                'id_order'      => $params['objOrder']->id
            ));

            if (isset($params['objOrder']->reference) && !empty($params['objOrder']->reference))
                $this->smarty->assign('reference', $params['objOrder']->reference);
        }
        else
            $this->smarty->assign('status', 'failed');
        
        return $this->display(__FILE__, 'payment_return.tpl');
	}

    public function checkCurrency($cart) {

        $currency_order = new Currency($cart->id_currency);
        $currencies_module = $this->getCurrency($cart->id_currency);

        if (is_array($currencies_module))
            foreach ($currencies_module as $currency_module)
                if ($currency_order->id == $currency_module['id_currency'])
                    return true;
        return false;
    }    
        
}

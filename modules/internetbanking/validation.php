<?php

/*
*
* Deprecated in PS 1.5
*
*
*/

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../header.php');

//Include internet banking
include(dirname(__FILE__).'/internetbanking.php');
$ibank = new Internetbanking();

$currency = new Currency(intval(isset($_POST['currency_payement']) ? $_POST['currency_payement'] : $cookie->id_currency));
$customer = new Customer((int)$cart->id_customer);

if (!$cart->checkQuantities()) {
    Tools::redirect('order.php');
}
else {
        $total = floatval(number_format($cart->getOrderTotal(true, 3), 2, '.', ''));

        /* Lionel of Ripplewerkz: Entered mail variable {ibanking_url} to hold pass url of the verification page to the customers email.
        Next step is to retrieve the banking details and pass it via a mailVar instead of hardcoding it into the email template like how it is now */

        //Get bank info
        $banks = $ibank->get_banks();

        $bank_details = "";
        foreach ($banks as $bank){
                $bank_details .= "<p><strong>".$bank['bank']."</strong><br>Account Number: ".$bank["number"]."<br>Account Type: ".$bank["type"];
                if ($bank['additional'] !=""){
                        $bank_details .= "<br>".$bank['additional'];
                }
                $bank_details .="</p>";

                $bank_details_text = "\r\n".$bank['bank']."\r\nAccount Number: ".$bank["number"]."\r\nAccount Type: ".$bank["type"];
                if ($bank['additional'] !=""){
                        $bank_details_text .= "\r\n".$bank['additional'];
                }
                $bank_details_text .="\r\n";
        }

	$mailVars = array(
		'{blank}' => 'this is blank',
		'{secure_key}' => $customer->secure_key,
		'{bank_details}' => $bank_details,
		'{bank_details_text}' => $bank_details_text,
		'{ibanking_url}' => Configuration::get('i_banking_url') . 'update.php?order='		
	);

	$ibank->validateOrder($cart->id, Configuration::get('ibank_status_transfer'), $total, 'Internet banking', NULL, $mailVars, $currency->id, false, $customer->secure_key);
	$order = new Order($ibank->currentOrder);

	Tools::redirectLink(Configuration::get('i_banking_url') . 'update.php?order=' . $order->id . '&secure=' . $order->secure_key);
        
}
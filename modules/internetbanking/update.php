<?php
require_once('../../config/config.inc.php');
require_once('../../header.php');

//Make sure that the order and secure string is valid
$order = $_GET['order'];
$order = new Order($order);
$secure = $order->secure_key;

// Get user id of the order
$id_customer = $order->id_customer;

//Get the internet banking
include('internetbanking.php');
$ibanking = new Internetbanking();

if (!isset($cookie->id_customer)) {
    Tools::redirect('authentication.php?back='.urlencode('modules/internetbanking/update.php?order='.$order->id));
}
elseif($cookie->id_customer != $id_customer) {
    echo 'You are not authorized to access this order. [' . $cookie->id_customer . ']';	
}
else {	
    
    $id_cart = $order->id_cart;
    $total = $order->total_paid_real;

    // Get the current status ID
    // There is a valid order, but is it of the correct status? (i.e. not cancelled / already paid)
    $order_state = $order->getCurrentState();
    
    //Get awaiting transfer status ID
    $transfer_state = (int) Configuration::get('ibank_status_transfer');

    //Get awaiting verification status ID
    $verification_state = (int) Configuration::get('ibank_status_verification');

    //Filter order states
    $display = false;
    $status_message = 'Invalid payment status';
    if ($order_state == 2) {
        $status_message= '
        <p>This order has already been verified.</p>
        ';
        $display = false;
    }
    if ($order_state == 6) {
        $status_message= "
        <p>Your order has been cancelled. Kindly re-add items to your cart if you'd like to purchase again.</p>
        ";
        $display = false;
    }

    if ($order_state == $verification_state) {
        $status_message= '
        <p>Your order is already pending payment verification, we will verify your payment and get back to you shortly!
        <br><br>
        If you happen to key in the wrong details by accident, do fill up the form again.</p>
        ';
        $display = true;
    }
    if ($order_state == $transfer_state) {
        $status_message = '';
        $display = true;
    }
    
    if ($display == false) {
        exit($status_message);
    }

    if ($display == true) {

        //Submit the form if button clicked
        $submit = false;
        if (isset($_POST['submit'])) {
            $submit = true;
            $new_date = date("Y-m-d H:i:s");

            //Upload image
            $file = $_FILES['trans-upload'];
            $filepath = false;
            $root = '';

            //Image validation
            if ((
            ($file["type"] == "image/gif")
            || ($file["type"] == "image/jpeg")
            || ($file["type"] == "image/pjpeg")
            || ($file["type"] == "application/pdf")
            ) &&
            $file["size"] <= 5242880
            ) {
                //Get file ext
                $ext = explode('.',$file['name']);
                $ext = $ext[count($ext)-1];
                //Generate unique filename
                $filename = md5(microtime() . rand()).'.'.$ext;
                //Copy temp file to new folder

                move_uploaded_file($file['tmp_name'], $root.'uploads/'.$filename);

                $filepath = Configuration::get('i_banking_url').'uploads/'.$filename;
            }
            
            //Message
            $message = addslashes("The user has paid by Internet Banking and updated their transfer details. <br />Order ID: ".$order->id."<br />Bank account transfer to: ".$_POST['bank']."<br />Mode of Payment: ".$_POST['payment_mode']."<br />Transaction Reference No.: ".$_POST['trans-id']." <br />Transaction Date: ".$_POST['trans-date']."<br />Transaction Time ".$_POST['trans-time']."<br />Nickname: ".$_POST['trans-nick']);
            
            //Add upload link to message if it exists
            if (!empty($filepath)) {
                $message .= "<br />Upload: <a href=\"".$filepath."\" style=\"text-decoration: underline;\">View upload</a>";
            }
            
            //Append the message to the order
            $sql = "INSERT INTO "._DB_PREFIX_."message (id_cart, id_customer, id_employee, id_order, message, private, date_add) VALUES ('$id_cart', '$id_customer', '1', '$order->id', '$message', '0', '$new_date')";
            if (Db::getInstance()->Execute($sql) == false) {
                echo 'Database error.'.mysql_error();
                exit();
            }
            
            //Send email
            $the_customer = new Customer($id_customer);
            $customers_email = $the_customer->email;
            $customers_name = $the_customer->firstname." ".$the_customer->lastname;

            //Fill this out with correct information of the client's internet banking
            $mail_headers = "From: Payment Verification <".Configuration::get('PS_SHOP_EMAIL').">\r\n" . "Reply-To: ".$customers_name." <".$customers_email.">\r\n" . "X-Mailer: PHP/" . phpversion();
            $mail_message = "From: ".$customers_name."\r\nEmail: ".$customers_email."\r\n \r\n".str_replace("<br />","\r\n",$message);
            mail(Configuration::get('PS_SHOP_EMAIL'),"Internet Banking Payment details submitted", str_replace("<br />","\r\n",$mail_message),$mail_headers);

            //Change the order status
            $history = new OrderHistory();
            $history->id_order = $order->id;
            $history->changeIdOrderState($verification_state, $order->id);
            $history->add();

            //Append the banking nick to the customer
            $ibanking->append_nickname($_POST['trans-nick'], $id_customer);
            
            //Append the payment method to the order
            $ibanking->append_method($_POST['payment_mode'], $order->id);

        }

        //Get bank info
        $banks =  $ibanking->get_banks();

        //Load template
        
        global $smarty;
        $smarty->assign(array(
            'shop_email' => Configuration::get('PS_SHOP_EMAIL'),
            'total' => $total,
            'bank' => $banks,
            'PHP_SELF' => $_SERVER['PHP_SELF'] . '?order=' . $order->id . '&secure=' . $secure,
            'submit' => $submit,
            'status_message' => $status_message
        ));
        $smarty->display(_PS_MODULE_DIR_.'internetbanking/payment_update.tpl');

    }

}

require_once('../../footer.php');
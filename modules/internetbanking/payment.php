<?php

/*
*
* Deprecated in PS 1.5
*
*
*/

/* SSL Management */
$useSSL = true;

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../header.php');
include(dirname(__FILE__).'/internetbanking.php');

if (!$cookie->isLogged())
    Tools::redirect('authentication.php?back=order.php');
// Execute order only if there's sufficient stock
if (!$cart->checkQuantities())
{
	Tools::redirect('order.php');
}else{	
	$ibank = new Internetbanking();
	echo $ibank->execPayment($cart);
}
include_once(dirname(__FILE__).'/../../footer.php');

?>

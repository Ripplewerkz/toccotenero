<?php
require_once(_PS_ROOT_DIR_.'/modules/prestamailchimp/classes/MCAPI.class.php');
require_once(_PS_ROOT_DIR_.'/modules/prestamailchimp/classes/Array.php');
require_once(_PS_ROOT_DIR_.'/modules/prestamailchimp/classes/Exception.php');
class PrestaMailchimp extends Module
{	
	function __construct()
	{
	 	$this->name = 'prestamailchimp';
		$this->tab = version_compare(_PS_VERSION_, '1.4.0.0', '>=')?'advertising_marketing':'Mediacom87';
		$this->version = '1.6';
		$this->author = 'Mediacom87';
		$this->need_instance = 1;
		$this->module_key = '270ec7d25bbc7e183054d0eb2d1d423f';
        $this->displayName = $this->l('Synchronize Prestashop and Mailchimp List');
		
	 	parent::__construct();
		
		$this->page = basename(__FILE__, '.php');
		if (!Configuration::get('MC_API_KEY'))
			$this->warning = $this->l('You have not yet set your MailChimp API Key!');
			
        $this->description = $this->l('synchronize your list with your clients');
		$this->confirmUninstall = $this->l('Are you sure you want to delete your details ?');
		$this->key = Configuration::get('MC_API_KEY');
	}
	
    function install()
    {
        if (!parent::install()
			OR !Configuration::updateValue('MC_CRON', 'no')
			OR !Configuration::updateValue('MC_API_KEY', '')
			OR !Configuration::updateValue('MC_LIS_ID', '')
			OR !Configuration::updateValue('MC_LIS_CUSTOMERS_ID', '')
			OR !Configuration::updateValue('MC_LIS_BAD_CUSTOMERS_ID', '')
			OR !Configuration::updateValue('MC_LIST_DELAY', '')
			OR !Configuration::updateValue('MC_LIS_BAD_GROUP_', '')
			OR !Configuration::updateValue('MC_LIS_NEWSLETTER_ID', '')
			OR !Configuration::updateValue('MC_LIS_NEWSCUST_ID', '')
			)
			return false;
		return true;
    }
	
	function uninstall()
	{
		if (!Configuration::deleteByName('MC_CRON') OR
		    !Configuration::deleteByName('MC_API_KEY') OR 
		    !Configuration::deleteByName('MC_LIS_ID') OR
		    !Configuration::deleteByName('MC_LIS_CUSTOMERS_ID') OR
		    !Configuration::deleteByName('MC_LIS_BAD_CUSTOMERS_ID') OR
		    !Configuration::deleteByName('MC_LIST_DELAY') OR
		    !Configuration::deleteByName('MC_LIS_BAD_GROUP_') OR
		    !Configuration::deleteByName('MC_LIS_NEWSLETTER_ID') OR
		    !Configuration::deleteByName('MC_LIS_NEWSCUST_ID') OR
            !parent::uninstall())
			return false;
		return true;
	}
	
	public function getContent()
	{
		global $cookie, $er;
		
		$output = '<h2>MailChimp</h2>';
		if (Tools::isSubmit('submitMCP'))
		{
		    $msg = $this->validate();
			
	        if ($msg == ''){
			    $output .= '
			    <div class="conf confirm">
				    <img src="../img/admin/ok.gif" alt="" title="" />
				    '.$this->l('API updated').'
			    </div>';
				$this->key = Tools::getValue('mc_api_key');
			} else {
			    $output .= '
			    <div class="alert ">
				    <img src="../img/admin/disabled.gif" alt="" title="" />
				    '.$msg.'
			    </div>';			
			}
			
			Configuration::updateValue('MC_CRON', Tools::getvalue('mc_cron'));
			if (Configuration::get('MC_CRON') == 'yes')
			{
				$cronModule = Module::getInstanceByName('cron');
				if (Validate::isLoadedObject($cronModule) && !$cronModule->cronExists($this->id, 'cron'))
					$cronModule->addCron($this->id, 'cron', '30 0 * * *');
			}
		}
		if (Tools::isSubmit('submitList'))
		{
			Configuration::updateValue('MC_LIS_ID', Tools::getValue('mc_list_id'));
			
			Configuration::updateValue('MC_LIS_CUSTOMERS_ID', Tools::getValue('mc_list_customers_id'));
						
			Configuration::updateValue('MC_LIS_BAD_CUSTOMERS_ID', Tools::getValue('mc_list_bad_customers_id'));		
			Configuration::updateValue('MC_LIST_DELAY', Tools::getValue('mc_list_delay'));
			
			$groups = Group::getGroups((int) $cookie->id_lang);
			foreach ($groups as $gr)
			{
				$mc_list_group_id = Tools::getValue('mc_list_group_'.$gr['id_group'].'');			
				Configuration::updateValue('MC_LIS_BAD_GROUP_'.$gr['id_group'].'', $mc_list_group_id);
			}
			
			Configuration::updateValue('MC_LIS_NEWSLETTER_ID', Tools::getValue('mc_list_newsletter_id'));
			
			Configuration::updateValue('MC_LIS_NEWSCUST_ID', Tools::getValue('mc_list_newscust_id'));
			
			$output .= '
			    <div class="conf confirm">
				    <img src="../img/admin/ok.gif" alt="" title="" />
				    '.$this->l('Lists updated').'
			    </div>';
		}
		if (Tools::isSubmit('submitSync'))
		{
			
			if (Configuration::get('MC_LIS_ID')) {
				$users = array();
				if (version_compare(_PS_VERSION_, '1.5.0.0', '>='))
					$newsletter = $this->_getCustomers('true');
				else
					$newsletter = Customer::getNewsletteremails();
				if (is_array($newsletter))
				{
					foreach ($newsletter as $n) $users[] = array('EMAIL' => $n['email'], 'FNAME' => $n['firstname'], 'LNAME' => $n['lastname']);
					
					$list = Configuration::get('MC_LIS_ID');
					$Synchronizer = new Galahad_MailChimp_Synchronizer_Array($this->key, $users);
					$Synchronizer->sync($list, true);
				}
			}
			
			if (Configuration::get('MC_LIS_CUSTOMERS_ID')) {
				$customers = Customer::getCustomers();
				if (is_array($customers))
				{
					foreach ($customers as $n)
						if (!Customer::isBanned($n['id_customer']))
							$all_users[] = array('EMAIL' => $n['email'], 'FNAME' => $n['firstname'], 'LNAME' => $n['lastname']);
					
					$list = Configuration::get('MC_LIS_CUSTOMERS_ID');
					$Synchronizer = new Galahad_MailChimp_Synchronizer_Array($this->key, $all_users);
					$Synchronizer->sync($list);
				}
			}
			
			if (Configuration::get('MC_LIS_BAD_CUSTOMERS_ID') AND Configuration::get('MC_LIST_DELAY')) {
				$bad_customers = $this->badCustomer();
				$bad_users = array();
				if (is_array($bad_customers) && $bad_customers)
				{
					foreach ($bad_customers as $n)
						if (!Customer::isBanned($n['id_customer']))
							$bad_users[] = array('EMAIL' => $n['email'], 'FNAME' => $n['firstname'], 'LNAME' => $n['lastname']);
					
					$list = Configuration::get('MC_LIS_BAD_CUSTOMERS_ID');
					$Synchronizer = new Galahad_MailChimp_Synchronizer_Array($this->key, $bad_users);
					$Synchronizer->sync($list);
				}
			}
			
			$groups = Group::getGroups((int) $cookie->id_lang);
			foreach ($groups as $gr)
			{
				$group_customers = array();
				$group_users = Array();
				if (Configuration::get('MC_LIS_BAD_GROUP_'.$gr['id_group'].'')) {
					$group_customers = $this->getCustomersGroup('true',$gr['id_group']);
					if (is_array($group_customers))
					{
						foreach ($group_customers as $n)
							if (!Customer::isBanned($n['id_customer']))
								$group_users[] = array('EMAIL' => $n['email'], 'FNAME' => $n['firstname'], 'LNAME' => $n['lastname']);
						
						$list = Configuration::get('MC_LIS_BAD_GROUP_'.$gr['id_group'].'');
						$Synchronizer = new Galahad_MailChimp_Synchronizer_Array($this->key, $group_users);
						$Synchronizer->sync($list);
					}
				}
			}
			
			if (Configuration::get('MC_LIS_NEWSLETTER_ID')) {
				$news_users = array();
				$bloc_newsletter = $this->getBlockNewsletter();
				if (is_array($bloc_newsletter) and $bloc_newsletter)
				{
					foreach ($bloc_newsletter as $n) $news_users[] = array('EMAIL' => $n['email']);
					
					$list = Configuration::get('MC_LIS_NEWSLETTER_ID');
					$Synchronizer = new Galahad_MailChimp_Synchronizer_Array($this->key, $news_users);
					$Synchronizer->sync($list, true);
				}
			}
			
			if (Configuration::get('MC_LIS_NEWSCUST_ID')) {
				$users = array();
				if (version_compare(_PS_VERSION_, '1.5.0.0', '>='))
					$news = $this->_getCustomers('true');
				else
					$news = Customer::getNewsletteremails();
					
				foreach ((array)$news as $n) $users[] = array('EMAIL' => $n['email'], 'FNAME' => $n['firstname'], 'LNAME' => $n['lastname']);
				
				$blocnews = $this->getBlockNewsletter();
				
				foreach ((array)$blocnews as $n) $users[] = array('EMAIL' => $n['email'], 'FNAME' => '', 'LNAME' => '');
				
				if (is_array($users) and $users)
				{
					$list = Configuration::get('MC_LIS_NEWSCUST_ID');
					$Synchronizer = new Galahad_MailChimp_Synchronizer_Array($this->key, $users);
					$Synchronizer->sync($list, true);
				}
			}
			
			$output .= '
			    <div class="conf confirm">
				    <img src="../img/admin/ok.gif" alt="" title="" />
				    '.$this->l('Synchronization Done').'
			    </div>';
		}
		return $output.$this->displayForm();
	}

	public function displayForm()
	{
		global $cookie;
		global $er;
		
		$output = '
			<fieldset style="float: right; width: 255px">
				<legend>'.$this->l('About').'</legend>
				<p style="font-size: 1.2em; font-weight: bold; padding-bottom: 0"><a href="http://eepurl.com/blWhX" target="_blank" title="'.$this->l('Subscribe').'"><img src="//www.mailchimp.com/img/badges/banner2.gif" alt="'.$this->displayName.'" style="float: left; padding-right: 1em"/></a>'.$this->displayName.'</p>
				<p style="clear: both">
				'.$this->description.'
				</p>
				<ol style="list-style:none">
					<li>1. '.$this->l('I create an account on').' <a href="http://eepurl.com/blWhX" target="_blank" style="color:#268CCD">MailChimp</a></li>
					<li>2. '.$this->l('I record my API key').'</li>
					<li>3. '.$this->l('I create my lists based on the scenarios (one unique list per scenario)').'</li>
					<li>3. '.$this->l('I select my lists').'</li>
					<li>4. '.$this->l('I sync').'</li>
				</ol>
				<p>
				'.$this->l('Developped by').' <a style="color: #900; text-decoration: underline;" href="http://www.mediacom87.fr">Mediacom87</a>'.$this->l(', which helps you develop your e-commerce site.').'
				</p>
				<p>
				<a href="http://www.mediacom87.fr/contact/"><img src="../img/admin/email.gif" alt="" /> '.$this->l('Contact').'</a>
				</p>
			</fieldset>';
		$output .= '
		<form action="'.$_SERVER['REQUEST_URI'].'" method="post">
			<fieldset class="width3">
				<legend><img src="../img/admin/cog.gif" alt="" class="middle" />'.$this->l('Settings').'</legend>
				<label>'.$this->l('Your API Key').'</label>
				<div class="margin-form">
					<input type="text" name="mc_api_key" value="'.$this->key.'" size="36" />
					<p class="clear"><a href="http://admin.mailchimp.com/account/api" class="green" target="_blank">'.$this->l('Go to your MailChimp Account to find this').'</a></p>
				</div>
				<hr size="1">';
				$module = Module::getInstanceByName('cron');
				if (Validate::isLoadedObject($module) &&
					$module->active)
				{
					$output .= '
					<label>'.$this->l('Update by Cron:').'</label>
					<div class="margin-form" style="margin-left:5px">
					<select name="mc_cron" title="'.$this->l('Choose Yes to set up a cron every day at half past midnight to automatically update your exports').'">
						<option value="yes"';
						if (Configuration::get('MC_CRON') == 'yes') $output .= ' selected="selected"';
						$output .= '>'.$this->l('Yes').'</option>
						<option value="no"';
						if (Configuration::get('MC_CRON') == 'no') $output .= ' selected="selected"';
						$output .= '>'.$this->l('No').'</option>
					</select>
					</div>';
				} else {
					$output .= '
					<p style="text-align:center">'.$this->l('Please install the crontab on your site to enjoy a daily update of your feeds').'</p><p style="text-align:center"><img src="'.$this->_path.'img/logo-store.png" /><a href="http://www.moncompteur.com/compteurclick.php?idLink=25066" target="_blank" style="color:#7BA45B;text-decoration:underline">Crontab</a></p>
					<p style="clear: both;">'.$this->l('Otherwise, you can also create a cron job on your server by calling the file').'<br /> '.dirname(__FILE__).'/cron.php '.$this->l('everyday').'</p>
					<p>'.$this->l('For exemple : ').'<br />'.$this->l('2am daily').'<br />0 	2 	* 	* 	* 	php -d '.dirname(__FILE__).'/cron.php</p>
					';
				}
				$output .= '
				<center><input type="submit" name="submitMCP" value="'.$this->l('Update Key').'" class="button" /></center>			
			</fieldset>
		</form>';
		
        if (Configuration::get('MC_API_KEY') || $this->key)
		{
			$output .= '
			<form action="'.$_SERVER['REQUEST_URI'].'" method="post">
				<fieldset class="width3 space">
					<p>'.$this->l('To synchronize your lists, it must be created in advance on your MailChimp account and then make the match by selecting the cases described below.').'</p>
					<p>'.$this->l('You should involve only one list per case. The case can not be combined on one list MailChimp.').'</p>
					<p>'.$this->l('You are not obliged to match each case to a list and avoid overloading your MailChimp account.').'</p>
					
					<hr size="1">
					
					<legend><img src="../img/admin/cog.gif" alt="" class="middle" />'.$this->l('List Configuration').'</legend>
					<label style="width:250px;margin-right:5px">'.$this->l('Customers newsletter subscribers List').'</label>
					<div class="margin-form" style="padding-left:0px">
						<select name="mc_list_id" style="width:300px">
						<option></option>';
						$api = new MCAPI(Configuration::get('MC_API_KEY'));
						$retval = $api->lists();
						if (!$api->errorCode){
							foreach ($retval as $list){
								$output .= '<option value="'.$list['id'].'"';
								if (Configuration::get('MC_LIS_ID') == $list['id'])
									$output .= ' selected="selected"';
								$output .= '>'.$list['name'].' - '.$list['member_count'].'</option>';
							}
						}
			$output .= '
						</select>
						<p class="clear">'.$this->l('The list of all your customers who subscribed to the newsletter in their profile').' - <a href="http://admin.mailchimp.com/lists/" class="green" target="_blank">'.$this->l('Go to your MailChimp Account to find this').'</a></p>
					</div>
					
					<hr size="1">
					<label style="width:250px;margin-right:5px">'.$this->l('Your customers List').'</label>
					<div class="margin-form" style="padding-left:0px">
						<select name="mc_list_customers_id" style="width:300px">
						<option></option>';
						if (!$api->errorCode){
							foreach ($retval as $list){
								$output .= '<option value="'.$list['id'].'"';
								if (Tools::getValue('mc_api_key', Configuration::get('MC_LIS_CUSTOMERS_ID')) == $list['id'])
									$output .= ' selected="selected"';
								$output .= '>'.$list['name'].' - '.$list['member_count'].'</option>';
							}
						}
			$output .= '
						</select>
						<p class="clear">'.$this->l('The list of all your clients').' - <a href="http://admin.mailchimp.com/lists/" class="green" target="_blank">'.$this->l('Go to your MailChimp Account to find this').'</a></p>
					</div>
					
					<hr size="1">
					<label style="width:250px;margin-right:5px">'.$this->l('Your customers List').'</label>
					<div class="margin-form" style="padding-left:0px">
						<select name="mc_list_bad_customers_id" style="width:300px">
						<option></option>';
						if (!$api->errorCode){
							foreach ($retval as $list){
								$output .= '<option value="'.$list['id'].'"';
								if (Configuration::get('MC_LIS_BAD_CUSTOMERS_ID') == $list['id'])
									$output .= ' selected="selected"';
								$output .= '>'.$list['name'].' - '.$list['member_count'].'</option>';
							}
						}
			$output .= '
						</select>
						<p style="width:65%;margin:.5em auto 0">'.$this->l('who have not made any orders for more than').' <input type="text" name="mc_list_delay" value="'.Tools::getValue('mc_list_delay', Configuration::get('MC_LIST_DELAY')).'" size="5" /> '.$this->l('days').'</p>
						<p class="clear">'.$this->l('The list of all your clients').' - <a href="http://admin.mailchimp.com/lists/" class="green" target="_blank">'.$this->l('Go to your MailChimp Account to find this').'</a></p>
					</div>
					<hr size="1">';
				
				$groups = Group::getGroups((int) $cookie->id_lang);
				foreach ($groups as $g)
				{			
					$output .= '
					<label style="width:250px;margin-right:5px">'.$this->l('Your Group List:').' '.$g['name'].'</label>
					<div class="margin-form" style="padding-left:0px">
						<select name="mc_list_group_'.$g['id_group'].'" style="width:300px">
						<option></option>';
						if (!$api->errorCode){
							foreach ($retval as $list){
								$output .= '<option value="'.$list['id'].'"';
								if (Configuration::get('MC_LIS_BAD_GROUP_'.$g['id_group']) == $list['id'])
									$output .= ' selected="selected"';
								$output .= '>'.$list['name'].' - '.$list['member_count'].'</option>';
							}
						}
			$output .= '
						</select>
						<p class="clear">'.$this->l('The list of all your clients for this Group').' - <a href="http://admin.mailchimp.com/lists/" class="green" target="_blank">'.$this->l('Go to your MailChimp Account to find this').'</a></p>
					</div>';
				}
				if (Module::isInstalled('blocknewsletter')) {
				$output .= '				
					<hr size="1">
					<label style="width:250px;margin-right:5px">'.$this->l('Your Block Newsletter List').'</label>
					<div class="margin-form" style="padding-left:0px">
						<select name="mc_list_newsletter_id" style="width:300px">
						<option></option>';
						if (!$api->errorCode){
							foreach ($retval as $list){
								$output .= '<option value="'.$list['id'].'"';
								if (Configuration::get('MC_LIS_NEWSLETTER_ID') == $list['id'])
									$output .= ' selected="selected"';
								$output .= '>'.$list['name'].' - '.$list['member_count'].'</option>';
							}
						}
			$output .= '
						</select>
						<p class="clear">'.$this->l('The list of all registered to the newsletter without being a customer').' - <a href="http://admin.mailchimp.com/lists/" class="green" target="_blank">'.$this->l('Go to your MailChimp Account to find this').'</a></p>
					</div>';
				}
				
				if (Module::isInstalled('blocknewsletter')) {
				$output .= '				
					<hr size="1">
					<label style="width:250px;margin-right:5px">'.$this->l('Customers and Block Newsletter List').'</label>
					<div class="margin-form" style="padding-left:0px">
						<select name="mc_list_newscust_id" style="width:300px">
						<option></option>';
						if (!$api->errorCode)
							foreach ($retval as $list)
								$output .= '<option value="'.$list['id'].'"'.(Configuration::get('MC_LIS_NEWSCUST_ID') == $list['id'] ? ' selected="selected"' : "").'>'.$list['name'].' - '.$list['member_count'].'</option>';
			$output .= '
						</select>
						<p class="clear">'.$this->l('The list of all registered to the newsletter without being a customer with all customers who sign to received your newsletter').' - <a href="http://admin.mailchimp.com/lists/" class="green" target="_blank">'.$this->l('Go to your MailChimp Account to find this').'</a></p>
					</div>';
				}
					
					$output .= '
					<center><input type="submit" name="submitList" value="'.$this->l('Save').'" class="button" /></center>			
				</fieldset>
			</form>';
		}
		
		if (Configuration::get('MC_API_KEY') AND $this->_checkConfList())
		{
			$output .= '
			<form action="'.$_SERVER['REQUEST_URI'].'" method="post" onsubmit="$(\'#submitSync\').css(\'display\', \'none\').val(\''.$this->l('Wait ...').'\');">
				<fieldset class="width3 space">
					<legend><img src="../img/admin/cog.gif" alt="" class="middle" />'.$this->l('Synchronize').'</legend>
					<center><input type="submit" name="submitSync" value="'.$this->l('Synchronize').'" class="button" /></center>			
				</fieldset>
			</form>';
		}
		
		$output .= '
		<fieldset class="space">
			<legend><img src="../img/admin/unknown.gif" alt="" class="middle" />'.$this->l('Help').'</legend>
			 <h3>'.$this->displayName.'</h3>
			 <p>'.$this->l('To activate, please:').'</p>
			 <ol>
			 	<li><a href="http://admin.mailchimp.com/" class="green" target="_blank">'.$this->l('Go to your MailChimp account').'</a> / <a href="http://eepurl.com/blWhX" class="green" target="_blank">'.$this->l('Create your account').'</a></li>
			 	<li><a href="http://admin.mailchimp.com/lists/" class="green" target="_blank">'.$this->l('Create your list').'</a></li>
			 	<li>'.$this->l('Synchronize ...').'</li>
			</ol>
		</fieldset>';
		return $output;
	}
	
    function validate(){
        $valid = false;
        $key = Tools::getValue('mc_api_key');
        if (!Configuration::get('MC_API_KEY') && !$key){
            return 'You must enter your API key.';
        }
        
        if (!Configuration::get('MC_API_KEY')){
            //$GLOBALS["mc_api_key"] = $key;
            $api = new MCAPI($key);
            $res = $api->ping();
            if ($api->errorMessage!=''){
                return $this->l('Server said:').' "'.$api->errorMessage.'". '.$this->l('Your API key is likely invalid. Please read the installation instructions.');
            } else {
    			Configuration::updateValue('MC_API_KEY', $key);
            }
        }
        return '';
    }
    
    function is_valid(){
        return $this->validate()==''?true:false;
    }
    
    function _checkConfList()
    {
    	if (Configuration::get('MC_LIS_ID') OR Configuration::get('MC_LIS_CUSTOMERS_ID') OR Configuration::get('MC_LIS_BAD_CUSTOMERS_ID') OR Configuration::get('MC_LIS_NEWSLETTER_ID') OR Configuration::get('MC_LIS_NEWSCUST_ID'))
    		return true;
    	global $cookie;
    	$groups = Group::getGroups((int) $cookie->id_lang);
		foreach ($groups as $g)
			if (Configuration::get('MC_LIS_BAD_GROUP_'.$g['id_group']))
				return true;
    	return false;
    }
    
    /* For all customers with no orders since more than x days */
	private function badCustomer()
	{
		$result = Db::getInstance()->ExecuteS('
		SELECT c.id_lang, c.id_cart, cu.id_customer, cu.firstname, cu.lastname, cu.email, (SELECT COUNT(o.id_order) FROM '._DB_PREFIX_.'orders o WHERE o.id_customer = cu.id_customer) nb_orders
		FROM '._DB_PREFIX_.'customer cu
		LEFT JOIN '._DB_PREFIX_.'orders o ON (o.id_customer = cu.id_customer)
		LEFT JOIN '._DB_PREFIX_.'cart c ON (c.id_cart = o.id_cart)
		WHERE cu.id_customer NOT IN
		(SELECT o.id_customer FROM '._DB_PREFIX_.'orders o WHERE DATE_SUB(CURDATE(),INTERVAL '.intval(Configuration::get('MC_LIST_DELAY')).' DAY) <= o.date_add)
		GROUP BY cu.id_customer
		HAVING nb_orders >= 1');
	
		return $result;
	}
	
	private function getCustomersGroup($newsletter = true, $id_group = 0)
    {
        $return = Db::getInstance()->ExecuteS('
			SELECT
				c.id_customer,
				c.email,
				c.lastname,
				c.firstname
			FROM
				'._DB_PREFIX_.'customer c
			LEFT JOIN '._DB_PREFIX_.'customer_group cg ON c.id_customer = cg.id_customer
			WHERE 1 = 1'.
			($newsletter?' AND c.newsletter=1':'').
			((int) $id_group?' AND cg.id_group = '.(int) $id_group.' ':'').'
			GROUP BY c.id_customer'
            );
        return $return;
    }

	private function _getCustomers($mail = null)
	{
		$dbquery = new DbQuery();
		$dbquery->select('c.`lastname`, c.`firstname`, c.`email`')
				->from('customer', 'c')
				->groupBy('c.`email`');

		if ($mail)
			$dbquery->where('c.`newsletter` = 1');


		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($dbquery->build());

		return $result;
	}
    
    private function getBlockNewsletter()
	{
		$result = Db::getInstance()->ExecuteS('
		SELECT n.email
		FROM `'._DB_PREFIX_.'newsletter` n
		WHERE n.email NOT IN (SELECT cu.email FROM `'._DB_PREFIX_.'customer` cu WHERE cu.newsletter = 1)');
		return $result;
	}
	
	private function in_multiarray($elem, $array)
    {
        $top = sizeof($array) - 1;
        $bottom = 0;
        while($bottom <= $top)
        {
            if($array[$bottom] == $elem)
                return true;
            else
                if(is_array($array[$bottom]))
                    if($this->in_multiarray($elem, ($array[$bottom])))
                        return true;
                   
            $bottom++;
        }       
        return false;
    }
    
	
	public function cron() {
		$module = Module::getInstanceByName('prestamailchimp');
		if (Validate::isLoadedObject($module) &&
			$module->active) {
			$_POST['submitSync'] = 1;
			$module->getContent();
		}
	}
}
?>

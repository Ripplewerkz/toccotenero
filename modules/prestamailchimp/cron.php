<?php

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../init.php');
include(dirname(__FILE__).'/prestamailchimp.php');

$module = Module::getInstanceByName('prestamailchimp');
if (Validate::isLoadedObject($module) &&
$module->active) {
	$module = new PrestaMailchimp();
	$module->cron();
	die ('Sync MailChimp : OK');
}

?>
{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if isset($htmlitems) && $htmlitems}
	{if $hook == 'home'}
		<ul class="htmlcontent-home clearfix row">
		{foreach name=items from=$htmlitems item=hItem}
			<li class="htmlcontent-item-{$smarty.foreach.items.iteration|escape:'htmlall':'UTF-8'}">
				<div class="ho-fe-set bp-rel animated hiding" data-animation="fadeInUp" data-delay="100">
					<div class="ho-fe-img bp-img wide loader">
						{if $hItem.image}
							<img data-original="{$link->getMediaLink("`$module_dir`img/`$hItem.image`")}" class="item-img lazy" title="{$hItem.title|escape:'htmlall':'UTF-8'}" alt="{$hItem.title|escape:'htmlall':'UTF-8'}" width="{if $hItem.image_w}{$hItem.image_w|intval}{else}100%{/if}" height="{if $hItem.image_h}{$hItem.image_h|intval}{else}100%{/if}"/>
						{/if}
					</div>
					{if $hItem.html}
					<div class="ho-fe-capt bp-ab bp-center par-xlarge par">
						<div class="main bp-set">
							{$hItem.html}
							<a class="h-b-btn bp-set mt1" href="{$hItem.url}">shop now</a>
						</div>
					</div>
					{/if}
				</div>
			</li>
		{/foreach}
		</ul>
		<div class="clr"></div>
		</div>
		<div class="home-featured bp-center animated hiding" data-animation="fadeInUp" data-delay="100">
			<div class="auto">
				{foreach name=items from=$htmlitems_hf item=hItem}
				<div class="ho-fe-main bp-set ho-fe-set bp-rel animated hiding" data-animation="fadeInUp" data-delay="200">
					<div class="ho-fe-img bp-img wide loader"><img src="{$link->getMediaLink("`$module_dir`img/`$hItem.image`")}" class="item-img lazy" title="{$hItem.title|escape:'htmlall':'UTF-8'}" alt="{$hItem.title|escape:'htmlall':'UTF-8'}" width="{if $hItem.image_w}{$hItem.image_w|intval}{else}100%{/if}" height="{if $hItem.image_h}{$hItem.image_h|intval}{else}100%{/if}"/></div>
					<div class="ho-fe-capt bp-ab bp-center par-xlarge par">
						<div class="main bp-set">
							{if $hItem.html}
								{$hItem.html}
							{/if}
							{if $hItem.url}
								<a class="h-b-btn bp-set mt1" href="{$hItem.url}">read more</a>
							{/if}
						</div>
					</div>
				</div>
				{/foreach}
			</div>
		</div>
	{else}
		<div id="htmlcontent_{$hook|escape:'htmlall':'UTF-8'}"{if $hook == 'footer'} class="footer-block col-xs-12 col-sm-4"{/if}>
			<ul class="htmlcontent-home clearfix row">
				{foreach name=items from=$htmlitems item=hItem}
					{if $hook == 'left' || $hook == 'right'}
						<li class="htmlcontent-item-{$smarty.foreach.items.iteration|escape:'htmlall':'UTF-8'} col-xs-12">
					{else}
						<li class="htmlcontent-item-{$smarty.foreach.items.iteration|escape:'htmlall':'UTF-8'} col-xs-4">
					{/if}
							{if $hItem.url}
								<a href="{$hItem.url|escape:'htmlall':'UTF-8'}" class="item-link"{if $hItem.target == 1} onclick="return !window.open(this.href);"{/if} title="{$hItem.title|escape:'htmlall':'UTF-8'}">
							{/if}
								{if $hItem.image}
									<img src="{$link->getMediaLink("`$module_dir`img/`$hItem.image`")}" class="item-img {if $hook == 'left' || $hook == 'right'}img-responsive{/if}" title="{$hItem.title|escape:'htmlall':'UTF-8'}" alt="{$hItem.title|escape:'htmlall':'UTF-8'}" width="{if $hItem.image_w}{$hItem.image_w|intval}{else}100%{/if}" height="{if $hItem.image_h}{$hItem.image_h|intval}{else}100%{/if}"/>
								{/if}
								{if $hItem.title && $hItem.title_use == 1}
									<h3 class="item-title">{$hItem.title|escape:'htmlall':'UTF-8'}</h3>
								{/if}
								{if $hItem.html}
									<div class="item-html">
										{$hItem.html}
									</div>
								{/if}
							{if $hItem.url}
								</a>
							{/if}
						</li>
				{/foreach}
			</ul>
		</div>
	{/if}
{/if}
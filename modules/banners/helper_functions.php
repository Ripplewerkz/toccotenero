<?php

function banners_get_categories($categories = '') {
    
    $where = '';
    if ($categories != '') {
        $where = 'WHERE ';
        foreach(preg_split("/[\s]*[,][\s]*/", $categories) as $key => $value) {
            if ($key == 0) {
                $where .= "name='".$value."'";
            }
            else {
                $where .= " OR name='".$value."'";
            }
        }
        $where .= ' ';
    }

    $sql = "SELECT * FROM "._DB_PREFIX_."banners_categories ".$where."ORDER BY position";

    //SQL query execute
    $data = Db::getInstance()->ExecuteS($sql);

    //If failed
    if ($data == false) {
        return array();
    }

    //If there are records
    return $data;
}

function banners_get_banners_private($id = '', $link_name = '', $counter = '') {
    $andwhere = '';
    if ($link_name != '') {
        $andwhere = "AND link not like '%".$link_name."%' ";
    }

    $limit = '';
    if ($counter != '') {
        $limit = " limit ".(int)$counter;
    }

    $sql = "SELECT * FROM "._DB_PREFIX_."banners_items WHERE category='".$id."' ".$andwhere."ORDER BY position".$limit;

    if (!$data = Db::getInstance()->ExecuteS($sql)) {
        return array();
    }
    else {
        return $data;
    }
}

function banners_get_banners($categories = '', $template='default.tpl', $link_name = '', $count = '') {
    
    $data = banners_get_categories($categories);
    
    if(count($data) > 0) {
        foreach ($data as $key => $value) {
            $data[$key]['banners'] = banners_get_banners_private($data[$key]['id'], $link_name, $count);
            foreach ($data[$key]['banners'] as $key2 => $value2) {
                $data[$key]['banners'][$key2]['description'] = stripslashes($data[$key]['banners'][$key2]['description']);
                if($link_name)
                    $data[$key]['banners'][$key2]['link'] = "../".$data[$key]['banners'][$key2]['link'];
            }
        }
    }

    if ($template !== false) {

        //Return using Smarty template

        global $smarty;
        $protocol_link = (Configuration::get('PS_SSL_ENABLED') || Tools::usingSecureMode()) ? 'https://' : 'http://';
        $ps_base_url = _MODULE_DIR_.'/';

        $smarty->assign(array(
            'data' => $data,
            'img_path' => $ps_base_url.'banners/banner_img/',
            'star_path' => $ps_base_url.'banners/img/star.png',
            'module_path' => $ps_base_url.'banners'
        ));

        return $smarty->display($_SERVER['DOCUMENT_ROOT']._MODULE_DIR_.'banners/templates/'.$template);

    }
    else {

        //Return array
        return $data;

    }
}
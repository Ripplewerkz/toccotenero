<?php
//Banners module for Prestashop - Authored by Chris Harrison at Ripplewerkz, Singapore 2nd June 2010
class Banners extends Module
{
	
	function __construct() {
		$this->name = 'banners';
		parent::__construct();

		$this->tab = 'Ripplewerkz Pte Ltd';
		$this->version = '0.1';
		$this->displayName = 'Banners';
		$this->description = 'Upload and display banners on your site.';

                include_once($_SERVER['DOCUMENT_ROOT'].$this->_path.'helper_functions.php');
	}
	
	public function install() {
		if (!parent::install() || !$this->registerHook('header')) {
                    return false;
                }
		$sql1 = Db::getInstance()->Execute('
		CREATE TABLE '._DB_PREFIX_.'banners_categories (
			`id` int(6) NOT NULL AUTO_INCREMENT,
			`name` varchar(255) NOT NULL,
			`width` varchar(255) NOT NULL,
			`height` varchar(255) NOT NULL,
			`title` varchar(1) NOT NULL,
                        `description` text(0) NOT NULL,
                        `new_window` varchar(100) NOT NULL,
                        `position` int(6) NOT NULL,
			PRIMARY KEY(`id`)
		) ENGINE=MyISAM default CHARSET=utf8');
                if ($sql1) {
                    return Db::getInstance()->Execute('
                    CREATE TABLE '._DB_PREFIX_.'banners_items (
                            `id` int(6) NOT NULL AUTO_INCREMENT,
                            `category` int(6) NOT NULL,
                            `href` varchar(255) NOT NULL,
                            `title` varchar(255) NOT NULL,
                            `description` text(0) NOT NULL,
                            `link` varchar(255) NOT NULL,
                            `position` int(6) NOT NULL,
                            `new_window` varchar(100) NOT NULL,
                            PRIMARY KEY(`id`)
                    ) ENGINE=MyISAM default CHARSET=utf8');
                }
                else {
                    return false;
                }
	}

        public function uninstall()
 	{
 	 	if (!parent::uninstall()) {
                    return false;
                }
                Db::getInstance()->Execute('DROP TABLE '._DB_PREFIX_.'banners_categories');
 	 	return Db::getInstance()->Execute('DROP TABLE '._DB_PREFIX_.'banners_items');
 	}

        public function hookHeader() {
            include_once($_SERVER['DOCUMENT_ROOT'].$this->_path.'helper_functions.php');
            return;
        }

        public function upload($image, $ext) {

            //SERVER ROOT FOR UPLOADING FILES
            $server_root = $_SERVER['DOCUMENT_ROOT'];

            $target = $this->_path;
            $target = $server_root.$target.'banner_img/';
            
            $hash = md5(''.time().'+'.rand(10000, 99999).'');
            $hash .= '.'.$ext.'';
            $target .= $hash;

            if (move_uploaded_file($image, $target)) {
                return $hash;
            }
            else {
                echo "Upload error.";
                exit();
            }

        }

        public function cat_up($id) {

            //Get categories
            $data = banners_get_categories();

            //Get the position of ID
            foreach ($data as $key => $value) {
                if ($data[$key]['id'] == $id) {
                    $position = $data[$key]['position'];
                }
            }

            //Get the ID under the ID's position
            foreach ($data as $key => $value) {
                if ($data[$key]['position'] == $position-1) {
                    $below = $data[$key]['id'];
                }
            }

            //Update database
            $position = $position - 1;
            $sql = "UPDATE "._DB_PREFIX_."banners_categories
                SET position='".$position."'
                WHERE id='".$id."'";

            if (Db::getInstance()->Execute($sql) == false) {
                echo 'Database error.';
                exit();
            }

            $below_position = $position + 1;
            $sql = "UPDATE "._DB_PREFIX_."banners_categories
                SET position='".$below_position."'
                WHERE id='".$below."'";

            if (Db::getInstance()->Execute($sql) == false) {
                echo 'Database error.';
                exit();
            }
        }

        public function banner_up($id) {
            //Get banners
            $data = banners_get_banners_private($_GET['cat']);

            //Get the position of ID
            foreach ($data as $key => $value) {
                if ($data[$key]['id'] == $id) {
                    $position = $data[$key]['position'];
                }
            }

            //Get the ID under the ID's position
            foreach ($data as $key => $value) {
                if ($data[$key]['position'] == $position-1) {
                    $below = $data[$key]['id'];
                }
            }

            //Update database
            $position = $position - 1;
            $sql = "UPDATE "._DB_PREFIX_."banners_items
                SET position='".$position."'
                WHERE id='".$id."'";

            if (Db::getInstance()->Execute($sql) == false) {
                echo 'Database error.';
                exit();
            }

            $below_position = $position + 1;
            $sql = "UPDATE "._DB_PREFIX_."banners_items
                SET position='".$below_position."'
                WHERE id='".$below."'";

            if (Db::getInstance()->Execute($sql) == false) {
                echo 'Database error.';
                exit();
            }
        }

        public function banner_down($id) {
            //Get banners
            $data = banners_get_banners_private($_GET['cat']);

            //Get the position of ID
            foreach ($data as $key => $value) {
                if ($data[$key]['id'] == $id) {
                    $position = $data[$key]['position'];
                }
            }

            //Get the ID abovethe ID's position
            foreach ($data as $key => $value) {
                if ($data[$key]['position'] == $position+1) {
                    $above = $data[$key]['id'];
                }
            }

            //Update database
            $position = $position + 1;
            $sql = "UPDATE "._DB_PREFIX_."banners_items
                SET position='".$position."'
                WHERE id='".$id."'";

            if (Db::getInstance()->Execute($sql) == false) {
                echo 'Database error.';
                exit();
            }

            $above_position = $position - 1;
            $sql = "UPDATE "._DB_PREFIX_."banners_items
                SET position='".$above_position."'
                WHERE id='".$above."'";

            if (Db::getInstance()->Execute($sql) == false) {
                echo 'Database error.';
                exit();
            }
        }

        public function cat_down($id) {

            //Get categories
            $data = banners_get_categories();

            //Get the position of ID
            foreach ($data as $key => $value) {
                if ($data[$key]['id'] == $id) {
                    $position = $data[$key]['position'];
                }
            }

            //Get the ID above the ID's position
            foreach ($data as $key => $value) {
                if ($data[$key]['position'] == $position+1) {
                    $above = $data[$key]['id'];
                }
            }

            //Update database
            $position = $position + 1;
            $sql = "UPDATE "._DB_PREFIX_."banners_categories
                SET position='".$position."'
                WHERE id='".$id."'";

            if (Db::getInstance()->Execute($sql) == false) {
                echo 'Database error.';
                exit();
            }

            $above_position = $position - 1;
            $sql = "UPDATE "._DB_PREFIX_."banners_categories
                SET position='".$above_position."'
                WHERE id='".$above."'";

            if (Db::getInstance()->Execute($sql) == false) {
                echo 'Database error.';
                exit();
            }
        }

        public function insert_category($name = '', $width = '300', $height = '200', $title = '0', $description = '0') {
            //Check that blank entries have not been passed
            if ($name == '') {
                echo 'The category name must be set.';
                exit();
            }
            if (is_numeric($width) == false || is_numeric($height) == false) {
                echo 'Width and height must be numbers.';
                exit();
            }

            //Get position
            $sql = "SELECT position FROM "._DB_PREFIX_."banners_categories ORDER BY position DESC LIMIT 1";
            $result = Db::getInstance()->ExecuteS($sql);
            if ($result == false) {
                $position = 0;
            }
            else {
                $position = $result[0]['position'] + 1;
            }

            $sql = "INSERT INTO "._DB_PREFIX_."banners_categories (name,width,height,title,description,position) VALUES
                    ('".$name."','".$width."','".$height."','".$title."','".$description."','".$position."')";

            if (Db::getInstance()->Execute($sql) == false) {
                echo 'Database error.';
                exit();
            }
        }

        public function insert_banner($category = '', $image = '', $ext = '', $title = '', $description = '', $link = '', $new_window = '') {
            //Check that blank entries have not been passed
           /* if ($image == '') {
                echo 'You must upload an image';
                exit();
            }*/
            if($ext){
                $ext = strtolower($ext);

                if ($ext == 'png' || $ext == 'jpg' || $ext == 'jpeg' || $ext == 'gif') {
                    //Go
                }
                else {
                    echo 'Image must be a GIF, JPEG or PNG';
                    exit();
                }
            }

            if ($title == '') {
                echo 'You must specify a banner title';
                exit();
            }
            if ($description == '') {
                echo 'You must specify a banner description';
                exit();
            }

            if ($title == 'NULL') { $title = ''; }
            if ($description == 'NULL') { $description = ''; }
            if ($new_window == '1') { $new_window = '_blank'; }

            //Upload
            if($image && $ext)
                $image = $this->upload($image, $ext);
            else
                $image = '';

             //Get position
            $sql = "SELECT position FROM "._DB_PREFIX_."banners_items WHERE category='".$category."' ORDER BY position DESC LIMIT 1";
            $result = Db::getInstance()->ExecuteS($sql);
            if ($result == false) {
                $position = 0;
            }
            else {
                $position = $result[0]['position'] + 1;
            }
            
            $sql = "INSERT INTO "._DB_PREFIX_."banners_items (category,href,title,description,link,new_window,position) VALUES
                    ('".$category."','".$image."','".$title."','".pSQL($description)."','".$link."','".$new_window."','".$position."')";

            if (Db::getInstance()->Execute($sql) == false) {
                echo 'Database error.';
                exit();
            }
        }

        public function update_category($id = '', $name='', $width='', $height='', $title='0', $description='0') {
            //Check an ID has been given
            if ($id == '') {
                echo 'Database error: No ID specified to update.';
                exit();
            }
            //Check that blank entries have not been passed
            if ($name == '') {
                echo 'The category name must be set.';
                exit();
            }
            if (is_numeric($width) == false || is_numeric($height) == false) {
                echo 'Width and height must be numbers.';
                exit();
            }

            $sql = "UPDATE "._DB_PREFIX_."banners_categories
                SET name='".$name."', width='".$width."', height='".$height."', title='".$title."', description='".$description."'
                WHERE id='".$id."'";

            if (Db::getInstance()->Execute($sql) == false) {
                echo 'Database error.';
                exit();
            }
        }

        public function update_banner($id = '', $image = '', $ext = '', $title = '', $description = '', $link='', $new_window='') {
            //Check that blank entries have not been passed
            $title = trim($title);
            $description = trim($description);
            if ($title == '') {
                echo 'You must specify a banner title';
                exit();
            }
            if ($description == '') {
                echo 'You must specify a banner description';
                exit();
            }

            if ($title == 'NULL') { $title = ''; }
            if ($description == 'NULL') { $description = ''; }
            if ($new_window == '1') { $new_window = '_blank'; }

            if ($image != '') {
                $hash = $this->upload($image, $ext);
            }

            if ($image == '') {
                
                $sql = "UPDATE "._DB_PREFIX_."banners_items
                SET title='".$title."', description='".$description."', link='".$link."', new_window='".$new_window."'
                WHERE id='".$id."'";
            }
            else {
                $sql = "UPDATE "._DB_PREFIX_."banners_items
                SET href='".$hash."', title='".$title."', description='".$description."', link='".$link."', new_window='".$new_window."'
                WHERE id='".$id."'";
            }

            if (Db::getInstance()->Execute($sql) == false) {
                echo 'Database error.';
                exit();
            }
        }

        public function delete_category($id) {

            /*Reorder the positions of the other IDs*/
            $data = banners_get_categories();
            
            $change = false;
            foreach ($data as $key => $value) {
                if ($change == true) {
                    $new_position = $data[$key]['position'] - 1;
                    $this_id = $data[$key]['id'];
                    //Update database
                    $sql = "UPDATE "._DB_PREFIX_."banners_categories SET position='".$new_position."' WHERE id='".$this_id."'";
                    if (Db::getInstance()->Execute($sql) == false) { echo 'Database error.'; exit(); }
                }
                if ($data[$key]['id'] == $id) {
                    $change = true;
                }
            }

            //Delete category based on $id
            Db::getInstance()->Execute('DELETE FROM '._DB_PREFIX_.'banners_categories
                WHERE `id`=\''.$id.'\'');

            //Delete banners within the category
             Db::getInstance()->Execute('DELETE FROM '._DB_PREFIX_.'banners_items
                WHERE `category`=\''.$id.'\'');
        }

        public function delete_banner($id) {

            //Reorder the positions of the other IDs
            $data = banners_get_banners_private($_GET['cat']);

            $change = false;
            foreach ($data as $key => $value) {
                if ($change == true) {
                    $new_position = $data[$key]['position'] - 1;
                    $this_id = $data[$key]['id'];
                    //Update database
                    $sql = "UPDATE "._DB_PREFIX_."banners_items SET position='".$new_position."' WHERE id='".$this_id."'";
                    if (Db::getInstance()->Execute($sql) == false) { echo 'Database error.'; exit(); }
                }
                if ($data[$key]['id'] == $id) {
                    $change = true;
                }
            }
            
            //Delete category based on $id
            $sql = "DELETE FROM "._DB_PREFIX_."banners_items
                WHERE id='".$id."'";

            if (Db::getInstance()->Execute($sql) == false) {
                echo 'Database error.';
                exit();
            }
        }

        public function get_single_category($id = '') {
            $sql = "SELECT * FROM "._DB_PREFIX_."banners_categories WHERE id='".$id."'";
            
            if (!$data = Db::getInstance()->ExecuteS($sql)) {
                echo 'Database error.';
                exit();
            }
            return $data;
        }

        public function create_url($action) {
            
            $valid_actions = array('edit','delete_cat','ed_banner','del_banner','cat_up','cat_down','banner_up','banner_down','display');

            //Remove actions
            $present_url = $_SERVER['REQUEST_URI'];
            foreach ($valid_actions as $this_action) {
                $present_url = str_replace('&action='.$this_action.'', '', $present_url);
            }
            //Remove IDs
            $present_url = explode("&id=", $present_url);
            $present_url = $present_url[0];

            //Add action
            if (in_array($action, $valid_actions) == true) {
                return ''.$present_url.'&action='.$action.'';
            }
            else {
                return '';
            }
        }

        public function strip_vars($location) {
            $location = explode("?", $location);
            $location = $location[0];
            $location = ''.$location.'?tab='.$_GET['tab'].'&configure='.$_GET['configure'].'&token='.$_GET['token'].'';
            return $location;
        }

	public function getContent() {
                if (Tools::isSubmit('add_edit_category')) {
                        //Convert checkbox values
                        if (isset($_POST['title'])) { $_POST['title'] = "1"; } else { $_POST['title'] = "0"; }
                        if (isset($_POST['description'])) { $_POST['description'] = "1"; } else { $_POST['description'] = "0"; }

			//Check if this is an insert or an update
                        if ($_POST['action'] == 'insert') {
                            //Add results of form submit to the database
                            $this->insert_category($_POST['name'], $_POST['width'], $_POST['height'], $_POST['title'], $_POST['description']);
                        }
                        if ($_POST['action'] == 'update_category') {
                            //Update the results
                            $this->update_category($_POST['id'], $_POST['name'], $_POST['width'], $_POST['height'], $_POST['title'], $_POST['description']);
                        }
                        
                        //Returns to configuration page
                        $location = $_SERVER['REQUEST_URI'];
			header("Location: $location");
			exit();
		}

                if (Tools::isSubmit('add_edit_banner')) {
			//Check if this is an insert or an update
                        if (isset($_POST['title']) == false) { $_POST['title'] = 'NULL'; }
                        if (isset($_POST['description']) == false) { $_POST['description'] = 'NULL'; }
                        
                        //Get extension
                        $ext = pathinfo($_FILES['file']['name']);
                        $ext = $ext['extension'];

                        if ($_POST['action'] == 'insert') {
                            //Add results of form submit to the database
                            $this->insert_banner($_POST['category'], $_FILES['file']['tmp_name'], $ext, $_POST['title'], $_POST['description'], $_POST['link'], $_POST['new_window']);
                        }
                        if ($_POST['action'] == 'update_banner') {
                            //Update the results
                            $this->update_banner($_POST['id'], $_FILES['file']['tmp_name'], $ext, $_POST['title'], $_POST['description'], $_POST['link'], $_POST['new_window']);
                        }

                        //Returns to configuration page
                        $location = $_SERVER['REQUEST_URI'];
			header("Location: $location");
			exit();
		}
                
                if (isset($_GET['action']) == true && $_GET['action'] == 'cat_up' && isset($_GET['id']) == true) {
                        //Make category go up
                        $this->cat_up($_GET['id']);
                }
                if (isset($_GET['action']) == true && $_GET['action'] == 'cat_down' && isset($_GET['id']) == true) {
                        //Make category go down
                        $this->cat_down($_GET['id']);
                }

                if (isset($_GET['action']) == true && $_GET['action'] == 'banner_up' && isset($_GET['id']) == true) {
                        //Make banner go down
                        $this->banner_up($_GET['id']);

                        $category = $_GET['cat'];
                        $location = $this->strip_vars($_SERVER['REQUEST_URI']);
                        $location = ''.$location.'&action=display&id='.$category.'';
			header("Location: $location");
			exit();
                }
                if (isset($_GET['action']) == true && $_GET['action'] == 'banner_down' && isset($_GET['id']) == true) {
                        //Make banner go down
                        $this->banner_down($_GET['id']);
                        
                        $category = $_GET['cat'];
                        $location = $this->strip_vars($_SERVER['REQUEST_URI']);
                        $location = ''.$location.'&action=display&id='.$category.'';
			header("Location: $location");
			exit();
            }
                
            if (Tools::getValue('action') == 'delete_cat' && Tools::getValue('id') > 0) {
                $this->delete_category(Tools::getValue('id'));
            }

            if (isset($_GET['action']) == true && $_GET['action'] == 'del_banner' && isset($_GET['id']) == true) {
                $this->delete_banner($_GET['id']);
                $category = $_GET['cat'];
                $location = $this->strip_vars($_SERVER['REQUEST_URI']);
                $location = ''.$location.'&action=display&id='.$category.'';
		        header("Location: $location");
			    exit();
            }

                if (isset($_GET['action']) == true && $_GET['action'] == 'display') {
                    $data['category'] = $this->get_single_category($_GET['id']);
                    $data['category'] = $data['category'][0];
                    $data['banners'] = banners_get_banners_private($_GET['id']);
                    $html = $this->display_banners($data);
                    include('ripplewerkz_update.php');
                    return $html.$update;
                }
                else {
                    $data = banners_get_categories();
                    if ($data != false) {
                        $data['category'] = $data;
                    }

                    $html = $this->display_categories($data);
                    include('ripplewerkz_update.php');
                    return $html.$update;
                }
	}

	private function display_banners($data) {
                $url = $_SERVER['REQUEST_URI'];
                $url = str_replace('&action=display', '', $url);
                $this_width = "200";
                $html = '
                <script type="text/javascript">
                    $(document).ready(function() {
                        var url = "'.$url.'";
                        $("p a").each(function(index) {
                            if ($(this).find("img").attr("src") == "../img/admin/arrow2.gif") {
                                $(this).attr("href", url);
                                $(this).html("<img src=\"../img/admin/arrow2.gif\">Back to banner categories");
                            }
                        });
                        $(".action_edit").click(function() {
                            rel = $(this).attr("rel");
                            rel = rel.split("|||");
                            $("#id").val(rel[0]);
                            $("#title").val(rel[1]);
                            $("#description").val(rel[2]);';
                            if($data['category']['id']==8)
                            {
                                $html .= 'var stars_val = rel[3];
                                        if(stars_val == ""){
                                            stars_val = 5;
                                        }

                                    $("#so_stars").find("option").each(function( i, opt ) {
                                        if( opt.value === stars_val ) 
                                            $(opt).attr("selected", "selected");
                                        else{
                                            $(opt).removeAttr("selected");
                                        }
                                    });
                                ';
                            }
                            else
                                $html .= '$("#link").val(rel[3]);';

                            $html .= '
                            if (rel[4] == "") {
                                $("#new_window").removeAttr("checked");
                            }
                            else {
                                $("#new_window").attr("checked", "checked");
                            }

                            $("legend").html("Edit banner");

                            $("#add_edit_banner").attr("value", "Update");

                            $("form#frm_banners_addedit #action").attr("value", "update_banner");

                            if ($("#reset").length == 0) {
                                $("#add_edit_banner").after("<a id=\"reset\" href=\"javascript:reset()\"><img src=\"'.$this->_path.'/img/reset.png\"></a>");
                            }

                            return false;
                        });
                    });
                    function reset() {
                        $("#id").val("");
                        $("#title").val("");
                        $("#description").val("");
                        $("#link").val("");
                        $("#new_window").removeAttr("checked");
                        $("#reset").remove();
                        $("legend").html("Add banner");
                        $("#action").attr("value", "insert");
                        $("#add_edit_banner").attr("value", "Add");
                    }
                </script>
                <link rel="stylesheet" href="'.$this->_path.'css/ripplewerkz.css" type="text/css">
                <div id="ripplewerkz">
                    <a href="http://ripplewerkz.com">
                        <img src="'.$this->_path.'img/ripplewerkz/ripplewerkz.gif" alt="Ripplewerkz">
                    </a>
                    <h1>
                        <span class="word_one">Banners</span> &gt; <span class="word_two">'.$data['category']['name'].'</span>
                    </h1>
                </div>
                <table class="table" style="width:100%">
                    <thead>
                        <tr>
                            <th>Image</th>';
                            if ($data['category']['title'] == '1') {
                                if($data['category']['id']==8)
                                    $html .= '<th>Name/Position</th>';
                                else
                                    $html .= '<th>Title</th>';
                            }
                            if ($data['category']['description'] == '1') {
                            $html .= '<th>Description</th>';
                            }
                            if($data['category']['id']==8)
                                $html .= '<th>Stars</th>';
                            else
                                $html .= '<th>Link</th>';

                            $html .= '<th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        ';
                        if (is_array($data['banners']) && count($data['banners']) > 0) {
                        foreach ($data['banners'] as $key => $value) {
                            $html .= '
                        <tr>
                            <td style="width: '.$this_width.'px;">';
                            if($data['banners'][$key]['href'])
                                $html .= '<img src="'.$this->_path.'timthumb.php?src='.$this->_path.'banner_img/'.$data['banners'][$key]['href'].'&w=100" />';
                            
                            $html .= '</td>';
                            if ($data['category']['title'] == '1') {
                            $html .= '
                            <td>
                                '.$data['banners'][$key]['title'].'
                            </td>';
                            }
                            if ($data['category']['description'] == '1') {
                            $html .= '
                            <td>
                                '.$data['banners'][$key]['description'].'
                            </td>';
                            }
                            $html .= '
                            <td>
                                ';
                                if($data['category']['id']==8){
                                    if($data['banners'][$key]['link'])
                                    {
                                        $ctr_stars = 1;
                                        while($ctr_stars <= $data['banners'][$key]['link']){
                                             $html .= '<img src="'.$this->_path.'img/star.png" alt="Star" width="15" style="float:left;"/>';
                                            $ctr_stars++;
                                        }
                                    }
                                }
                                else
                                    $html .= $data['banners'][$key]['link'];

                            $html .= '</td>
                            <td>
                            ';
                            $edit_url = $this->create_url('ed_banner');
                            $delete_url = $this->create_url('del_banner');
                            $up_url = $this->create_url('banner_up');
                            $down_url = $this->create_url('banner_down');
                            $html .='
                            <p>
                                <a href="'.$edit_url.'&id='.$data['category']['id'].'">
                                    <img src="'.$this->_path.'img/edit.png" class="action_edit" rel="'.$data['banners'][$key]['id'].'|||'.$data['banners'][$key]['title'].'|||'.$data['banners'][$key]['description'].'|||'.$data['banners'][$key]['link'].'|||'.$data['banners'][$key]['new_window'].'" alt="Edit" style="margin-right: 10px;" alt="Edit" title="Edit">
                                </a>

                                '; if ($key == 0) { $grey = "_grey"; } else { $grey = ""; }
                                $html .= '
                                <a href="'.$up_url.'&id='.$data['banners'][$key]['id'].'&cat='.$data['category']['id'].'">
                                    <img src="'.$this->_path.'img/up'.$grey.'.png" alt="Up" title="Up" style="margin-right: 10px;">
                                </a>

                                '; if ($key == count($data['banners'])-1) { $grey = "_grey"; } else { $grey = ""; }
                                $html .= '
                                <a href="'.$down_url.'&id='.$data['banners'][$key]['id'].'&cat='.$data['category']['id'].'">
                                    <img src="'.$this->_path.'img/down'.$grey.'.png" alt="Down" title="Down" style="margin-right: 10px;">
                                </a>

                                <a href="'.$delete_url.'&id='.$data['banners'][$key]['id'].'&cat='.$data['category']['id'].'">
                                    <img src="'.$this->_path.'img/delete.png" alt="Delete" title="Delete">
                                </a>
                            </p>
                            </td>
                        </tr>
                        ';
                        }
                        }
                        $html .='
                    </tbody>
                </table>
                <br>
                <form id="frm_banners_addedit" action="'.$_SERVER['REQUEST_URI'].'" method="post" enctype="multipart/form-data">
                <fieldset>
                    <legend>Add ';
                    $html .= !empty($data['category']['name'])?$data['category']['name']:'Banner';
                    $html .='</legend>
                    <input type="hidden" name="category" id="category" value="'.$data['category']['id'].'" />
                    <input type="hidden" name="id" id="id" value="" />
                    <input type="hidden" name="action" id="action" value="insert" />
                    
                    <label for="file">Image ('.$data['category']['width'].'x'.$data['category']['height'].'px)</label>
                    <div class="margin-form">
                        <input type="file" name="file" id="file" />
                    </div>
                    <br />
                    ';
                    if (isset($data['category']['title']) == true && $data['category']['title'] == '1') {
                        
                    $html .= '<label for="title">';
                    if($data['category']['id']==8)
                        $html .= 'Name/Position';
                    else
                        $html .= 'Title';
                    $html .= '</label>
                    <div class="margin-form">
                        <input type="text" name="title" id="title" />
                    </div>';
                    }
                    if (isset($data['category']['description']) == true && $data['category']['description'] == '1') {
                    /*$html .= '
                    <label for="description">Description</label>
                    <div class="margin-form">
                        <input type="text" name="description" id="description" />
                    </div>';*/
                    $html .= '
                    <label for="description">Description</label>
                    <div class="margin-form">
                        <textarea name="description" id="description" style="width: 600px; height: 300px;"></textarea>
                    </div>';
                    }
                    $html .= '<label for="link">';
                    $max_stars = 5;
                    $ctr_stars = 1;
                    if($data['category']['id']==8)
                        $html .= 'Stars';
                    else 
                        $html .= 'Link';

                    $html .= '</label>
                    <div class="margin-form">';
                    if($data['category']['id']==8){
                        $html .= '<select name="link" id="so_stars">';
                        while($ctr_stars <= $max_stars){
                            $html .= '<option value="'.$ctr_stars.'"';
                            if($ctr_stars==$max_stars)
                                $html .= ' selected="selected"';
                            $html .= '>'.$ctr_stars.'</option>';
                            $ctr_stars++;
                        }
                        $html .= '</select>';
                    }
                    else
                        $html .= '<input type="text" name="link" id="link" />';

                    $html .= '</div>
                    <label for="new_window">Open new window</label>
                    <div class="margin-form">
                        <input type="checkbox" name="new_window" id="new_window" value="1" />
                    </div>
                    <input type="submit" name="add_edit_banner" id="add_edit_banner" value="Add" class="button" style="margin-right: 10px;" />
                </fieldset>
                </form>
                ';
                return $html;
        }
        private function display_categories($data) {
		//Get categories
                
                $html = '
                <script type="text/javascript">
                    $(document).ready(function() {
                        $(".action_edit").click(function() {
                            rel = $(this).attr("rel");
                            rel = rel.split("|||");
                            $("#id").val(rel[0]);
                            $("#name").val(rel[1]);
                            $("#width").val(rel[2]);
                            $("#height").val(rel[3]);
                            if (rel[4] == true) {
                                $("#title").attr("checked", true);
                            }
                            else {
                                $("#title").attr("checked", false);
                            }
                            if (rel[5] == true) {
                                $("#description").attr("checked", true);
                            }
                            else {
                                $("#description").attr("checked", false);
                            }

                            $("legend").html("Edit category ("+rel[1]+")");

                            $("#add_edit_category").attr("value", "Update");

                            $("#action").attr("value", "update_category");
                            
                            if ($("#reset").length == 0) {
                                $("#add_edit_category").after("<a id=\"reset\" href=\"javascript:reset()\"><img src=\"'.$this->_path.'img/reset.png\" alt=\"Reset\" title=\"Reset\"></a>");
                            }

                            return false;
                        });
                    });
                    function reset() {
                        $("#id").val("");
                        $("#name").val("");
                        $("#width").val("");
                        $("#height").val("");
                        $("#title").attr("checked", false);
                        $("#description").attr("checked", false);
                        $("#add_edit_category").attr("value", "Add");
                        $("#reset").remove();
                        $("legend").html("Add category");
                        $("#action").attr("value", "insert");
                    }
                </script>
                <link rel="stylesheet" href="'.$this->_path.'css/ripplewerkz.css" type="text/css">
                <div id="ripplewerkz">
                    <a href="http://ripplewerkz.com">
                        <img src="'.$this->_path.'img/ripplewerkz/ripplewerkz.gif" alt="Ripplewerkz">
                    </a>
                    <h1>
                        <span class="word_one">Banners</span>
                    </h1>
                </div>
                ';
                if ($data != false) {
                $html .='
                <table class="table" style="width:100%">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Width</th>
                            <th>Height</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        ';
                        foreach ($data['category'] as $key => $value) {
                            $display_url = $this->create_url('display');
                            $html .= '
                        <tr>
                            <td>
                                <a href="'.$display_url.'&id='.$data['category'][$key]['id'].'" style="text-decoration: underline;">'.$data['category'][$key]['name'].'</a>
                            </td>
                            <td>
                                '.$data['category'][$key]['width'].'px
                            </td>
                            <td>
                                '.$data['category'][$key]['height'].'px
                            </td>
                            <td>';
                            if ($data['category'][$key]['title'] == "1") {
                                $html .= 'Yes';
                            }
                            else {
                                $html .= 'No';
                            }
                            $html .= '
                            </td>
                            <td>';
                            if ($data['category'][$key]['description'] == "1") {
                                $html .= 'Yes';
                            }
                            else {
                                $html .= 'No';
                            }
                            $html .= '
                            </td>
                            <td>';
                            $edit_url = $this->create_url('edit');
                            $delete_url = $this->create_url('delete_cat');
                            $up_url = $this->create_url('cat_up');
                            $down_url = $this->create_url('cat_down');
                            $html .='
                            <p>
                                <a href="'.$edit_url.'&id='.$data['category'][$key]['id'].'">
                                    <img src="'.$this->_path.'img/edit.png" class="action_edit" rel="'.$data['category'][$key]['id'].'|||'.$data['category'][$key]['name'].'|||'.$data['category'][$key]['width'].'|||'.$data['category'][$key]['height'].'|||'.$data['category'][$key]['title'].'|||'.$data['category'][$key]['description'].'" alt="Edit" style="margin-right: 10px;" alt="Edit" title="Edit">
                                </a>

                                '; if ($key == 0) { $grey = "_grey"; } else { $grey = ""; }
                                $html .= '
                                <a href="'.$up_url.'&id='.$data['category'][$key]['id'].'">
                                    <img src="'.$this->_path.'img/up'.$grey.'.png" alt="Up" title="Up" style="margin-right: 10px;">
                                </a>
                                
                                '; if ($key == count($data['category'])-1) { $grey = "_grey"; } else { $grey = ""; }
                                $html .= '
                                <a href="'.$down_url.'&id='.$data['category'][$key]['id'].'">
                                    <img src="'.$this->_path.'img/down'.$grey.'.png" alt="Down" title="Down" style="margin-right: 10px;">
                                </a>

                                <a href="'.$delete_url.'&id='.$data['category'][$key]['id'].'">
                                    <img src="'.$this->_path.'img/delete.png" alt="Delete" title="Delete">
                                </a>
                            </p>
                            </td>
                        </tr>
                        ';
                        }
                        $html .='
                    </tbody>
                </table>
                <br>
                ';
                }
                $html .= '
                <form action="'.$_SERVER['REQUEST_URI'].'" method="post">
                <fieldset>
                    <legend>Add category</legend>
                    <input type="hidden" name="id" id="id" value="" />
                    <input type="hidden" name="action" id="action" value="insert" />
                    <label for="name">Category name</label>
                    <div class="margin-form">
                        <input type="text" name="name" id="name" />
                    </div>
                    <label for="width">Banner width</label>
                    <div class="margin-form">
                        <input type="text" name="width" id="width" />px
                    </div>
                    <label for="height">Banner height</label>
                    <div class="margin-form">
                        <input type="text" name="height" id="height" />px
                    </div>
                    <label for="text">Title</label>
                    <div class="margin-form" style="margin-top: 5px">
                        <input type="checkbox" name="title" id="title" value="1"/>
                    </div>
                    <label for="description">Description</label>
                    <div class="margin-form" style="margin-top: 5px">
                        <input type="checkbox" name="description" id="description" value="1"/>
                    </div>
                    <input type="submit" name="add_edit_category" id="add_edit_category" value="Add" class="button" style="margin-right: 10px;" />
                </fieldset>
                </form>
                <br>
                <fieldset>
                <legend>Helper functions</legend>
                <p>This module creates helper functions that can be used <b>anywhere</b> within Prestashop.</p>
                <h3>banners_get_banners( $categories , $template )</h3>
                <br>
                <p><b>$categories</b> <i>Comma seperated list</i></p>
                <p>Default: \'\' (includes all categories)</p>
                <p>List of all the categories to be included</p>
                <br>
                <p><b>$template</b> <i>String</i> / <i>Boolean</i></p>
                <p>Default: \'default.tpl\'</p>
                <p>The template file you want to use to display. If set to false the data will returned as an array.</p>
                </fieldset>
                ';

                return $html;
	}

}
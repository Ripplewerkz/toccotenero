{foreach from=$data item=cat}
    {if !empty($cat.banners)}
    <ul id="banner_cat_{$cat.id}">
        {foreach from=$cat.banners item=banner}
        <li>
            <div class="banner">
                {if $banner.link != ''}
                <a href="{$banner.link}"{if $banner.new_window != ''} target="{$banner.new_window}"{/if}>
                {/if}
                <img src="{$img_path}{$banner.href}" alt="{$banner.title}" width="{$cat.width}" height="{$cat.height}" />
                {if $banner.link != ''}
                </a>
                {/if}
                {if $cat.title == 1}
                <p class="banner_title">{if $banner.link != ''}<a href="{$banner.link}">{/if}{$banner.title}{if $banner.link != ''}</a>{/if}</p>
                {/if}
                {if $cat.title == 1}
                <p class="banner_description">{$banner.description}</p>
                {/if}
            </div>
        </li>
        {/foreach}
    </ul>
    {/if}
{/foreach}

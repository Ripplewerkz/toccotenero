 <div id="slider-wrapper">

{foreach from=$data item=cat}

    {if !empty($cat.banners)}

    	<ul id="banner_cat_{$cat.id}">

        	<li>

            <div id="slider" class="nivoSlider window">

                {foreach from=$cat.banners item=banner}

                    {if $banner.link != ''}

                        <a href="{$banner.link}"{if $banner.new_window != ''} target="{$banner.new_window}"{/if}>

                        {/if}

                        <img src="{$img_path}{$banner.href}" alt="{$banner.title}" width="{$cat.width}" height="{$cat.height}" />

                        {if $banner.link != ''}

                        </a>

                    {/if}

                {/foreach}

                

                    {if $cat.title == 1}

                    <p class="banner_title">{if $banner.link != ''}<a href="{$banner.link}">{/if}{$banner.title}{if $banner.link != ''}</a>{/if}</p>

                    {/if}

                    {if $cat.title == 1}

                    <p class="banner_description">{$banner.description}</p>

                    {/if}



            </div>

            <!-- end window-->

			

        </li>

        

    </ul>

    {/if}

{/foreach}

</div>
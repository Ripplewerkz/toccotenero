{foreach from=$data item=cat}
    {if !empty($cat.banners)}
    <div class="home-instagram bp-center pad-edge pb2">
        <div class="auto">
            <h1 class="ho-co-title">instagram</h1>
            <div class="h-c-insta par2 par-xlarge bp-tt">follow <a href="{$instagram_url}">@toccotenero</a></div>
            <div class="ho-ins-list bp-rel mt4 mb4">
                <ul>
                {foreach from=$cat.banners item=banner}
                    <li>
                        <div class="h-p-img bp-img wide">
                            <a href="{$banner.link}"{if $banner.new_window != ''} target="{$banner.new_window}"{/if}>
                                <img src="{$img_path}{$banner.href}" widht="{$cat.width}" height="{$cat.height}" alt="{$banner.title}" title="{$banner.title}" />
                            </a>
                        </div>
                    </li>
                {/foreach}
                </ul>
            </div>
        </div>
    </div>
    {/if}
{/foreach}
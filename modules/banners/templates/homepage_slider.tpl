<div class="home-banner bp-rel animated hiding" data-animation="fadeInUp" data-delay="100">
    {foreach from=$data item=cat}
        {if !empty($cat.banners)}
        <div class="flexslider nav2">
            <ul class="slides" id="banner_cat_{$cat.id}">
            {foreach from=$cat.banners item=banner}
                <li>
                    <div class="ho-ba-set bp-rel bp-topheight">
                        <div class="h-b-img bp-ab background-image-holder">
                            <img src="{$img_path}{$banner.href}" alt="{$banner.title}" width="{$cat.width}" height="{$cat.height}" />
                        </div>
                        <div class="h-b-capt bp-ab bp-middle pad-edge">
                            <div class="auto small">
                                <div class="h-b-ca-set fr par par-xxlarge">
                                    <h1>{$banner.title}</h1>
                                    <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus fells. </p>
                                    <div class="h-b-bottom">
                                        <a class="h-b-btn mt1" href="{$banner.link}"{if $banner.new_window != ''} target="{$banner.new_window}"{/if}>shop now</a>
                                    </div>
                                </div>
                                <div class="clr"></div>
                            </div>
                        </div>
                    </div>
                </li>
            {/foreach}
        </ul>
        {/if}
    {/foreach}
</div>
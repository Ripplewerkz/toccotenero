{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie7"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie8"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<!--[if gt IE 8]> <html class="no-js ie9"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<html{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}>
	<head>
		<meta charset="utf-8" />
		<title>{$meta_title|escape:'html':'UTF-8'}</title>
		{if isset($meta_description) AND $meta_description}
			<meta name="description" content="{$meta_description|escape:'html':'UTF-8'}" />
		{/if}
		{if isset($meta_keywords) AND $meta_keywords}
			<meta name="keywords" content="{$meta_keywords|escape:'html':'UTF-8'}" />
		{/if}
		<meta name="generator" content="PrestaShop" />
		<meta name="robots" content="{if isset($nobots)}no{/if}index,{if isset($nofollow) && $nofollow}no{/if}follow" />
		<meta name="viewport" content="width=device-width, minimum-scale=0.25, maximum-scale=1.0, initial-scale=1.0" />
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<link rel="icon" type="image/vnd.microsoft.icon" href="{$favicon_url}?{$img_update_time}" />
		<link rel="shortcut icon" type="image/x-icon" href="{$favicon_url}?{$img_update_time}" />
		{if isset($css_files)}
			{foreach from=$css_files key=css_uri item=media}
				{if $css_uri == 'lteIE9'}
					<!--[if lte IE 9]>
					{foreach from=$css_files[$css_uri] key=css_uriie9 item=mediaie9}
					<link rel="stylesheet" href="{$css_uriie9|escape:'html':'UTF-8'}" type="text/css" media="{$mediaie9|escape:'html':'UTF-8'}" />
					{/foreach}
					<![endif]-->
				{else}
					<link rel="stylesheet" href="{$css_uri|escape:'html':'UTF-8'}" type="text/css" media="{$media|escape:'html':'UTF-8'}" />
				{/if}
			{/foreach}
		{/if}
		{if isset($js_defer) && !$js_defer && isset($js_files) && isset($js_def)}
			{$js_def}
			{foreach from=$js_files item=js_uri}
			<script type="text/javascript" src="{$js_uri|escape:'html':'UTF-8'}"></script>
			{/foreach}
		{/if}
		{$HOOK_HEADER}
		<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:300,600&amp;subset=latin,latin-ext" type="text/css" media="all" />
		<!--[if IE 8]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<script src="{$js_dir}html5.js"></script>
		<script src="{$js_dir}lib/modernizr-2.8.3.min.js"></script>
		<script src="{$js_dir}pace.min.js"></script>
	</head>
	<body{if isset($page_name)} id="{$page_name|escape:'html':'UTF-8'}"{/if} class="{if isset($page_name)}{$page_name|escape:'html':'UTF-8'}{/if}{if isset($body_classes) && $body_classes|@count} {implode value=$body_classes separator=' '}{/if}{if $hide_left_column} hide-left-column{else} show-left-column{/if}{if $hide_right_column} hide-right-column{else} show-right-column{/if}{if isset($content_only) && $content_only} content_only{/if} lang_{$lang_iso}">
	<section id="main-container" class="clearfix">
		{if !isset($content_only) || !$content_only}
		<header>
			<div class="header-content">
				{capture name='displayBanner'}{hook h='displayBanner'}{/capture}
				{if $smarty.capture.displayBanner}
					<div class="header-top pad-edge">
						<div class="auto bp-rel">
							{$smarty.capture.displayBanner}
						</div>
					</div>
				{/if}
				<div class="header-middle pad-edge">
					<div class="auto bp-rel">
						<div class="menu-bar bp-ab"><span></span><span></span><span></span></div>
						<div class="logo bp-center">
							<a href="{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{else}{$base_dir}{/if}" title="{$shop_name|escape:'html':'UTF-8'}">
								<img class="lazy" src="{$logo_url}" data-original="{$logo_url}" alt="{$shop_name|escape:'html':'UTF-8'}"{*{if isset($logo_image_width) && $logo_image_width} width="{$logo_image_width}"{/if}{if isset($logo_image_height) && $logo_image_height} height="{$logo_image_height}"{/if}*}  width="244" height="68"/>
							</a>
						</div>
						<div class="logo-left fl"><a href="{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{else}{$base_dir}{/if}"><img src="{$tpl_images_uri}page_template/logo_t.png" width="45" height="63" alt="" title="" /></a></div>
						{if isset($HOOK_TOP)}{$HOOK_TOP}{/if}
						<div class="he-mi-right bp-ab bp-middle">
							<ul>
								<li class="h-m-search">
									<form id="searchbox" method="get" action="{$link->getPageLink('search', null, null, null, false, null, true)|escape:'html':'UTF-8'}" >
							<input type="hidden" name="controller" value="search" />
							<input type="hidden" name="orderby" value="position" />
							<input type="hidden" name="orderway" value="desc" />
							<input class="search_query form-control" type="text" id="search_query_top" name="search_query" placeholder="{l s='Search' mod='blocksearch'}" value="{if isset($search_query)}{$search_query|escape:'htmlall':'UTF-8'|stripslashes}{/if}" />
							<button type="submit" id="search_button"><i class="icon-search"></i></button>
						</form></li>
								<li class="h-m-my_account"><a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}"><span class="capt">my account</span></a></li>
								<li class="h-m-bag shopping_cart"><a href="{$link->getPageLink('order', true)|escape:'html':'UTF-8'}"><span class="ajax_cart_quantity icon bag">{$cart_qties}</span></a></li>

							</ul>
						</div>
						<div class="clr"></div>
					</div>
				</div>
			</div>
		</header>
		<!-- for sliding menu -->
		<div class="menu-sliding main">
			<div class="me-sl-main pad-edge bp-center">
				<div class="auto bp-rel">
					<div class="me-sl-close"><i class="fa fa-close"></i></div>
					<div class="me-sl-img bp-img">
						<a href="{$base_uri}"><img src="{$tpl_images_uri}page_template/logo_t.png" width="57" height="80" alt="" /></a>
					</div>
					{hook h='displayTopSliderMenu' id='top_slider'}
					{*}<div class="me-sl-right fr bp-tt par-medium">
						<ul>
							<li class="me-sl-search">
								<form id="searchbox" method="get" action="{$link->getPageLink('search', null, null, null, false, null, true)|escape:'html':'UTF-8'}" >
									<input type="hidden" name="controller" value="search" />
									<input type="hidden" name="orderby" value="position" />
									<input type="hidden" name="orderway" value="desc" />
									<input class="search_query form-control" type="text" id="search_query_top" name="search_query" placeholder="{l s='Search' mod='blocksearch'}" value="{if isset($search_query)}{$search_query|escape:'htmlall':'UTF-8'|stripslashes}{/if}" />
									<button type="submit" id="search_button"><i class="icon-search"></i></button>
								</form>
							</li>
							<li><a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}">my account</a></li>
						</ul>
					</div>{*}
					<div class="clr"></div>
				</div>
			</div>		
		</div>
		{/if}
		<!-- for sliding menu -->
	{if !isset($content_only) || !$content_only}
		{if isset($restricted_country_mode) && $restricted_country_mode}
			<div id="restricted-country">
				<p>{l s='You cannot place a new order from your country.'}{if isset($geolocation_country) && $geolocation_country} <span class="bold">{$geolocation_country|escape:'html':'UTF-8'}</span>{/if}</p>
			</div>
		{/if}
			<section id="main-wrapper" class="columns-container">
				{if $page_name !='index'}
					<div {if $page_name=='product'}class="inner-content animated hiding" data-animation="fadeInUp" data-delay="100"{else}class="inner-content"{/if}>
				{/if}
					{if $page_name=='category' && $category->description}
						<div class="pr-co-banner bp-center pad-edge">
							<div class="p-c-icon bp-set"><img class="lazy" data-original="{$tpl_images_uri}products/icon_banner_heart.png" alt="" /></div>
							<div class="clr"></div>
							<div class="p-c-icon bp-set vm"><img class="lazy" data-original="{$tpl_images_uri}products/icon_banner_wheel.png" alt="" /></div>
							<div class="p-c-main bp-set vm bp-box">
								{$category->description}
							</div>
							<div class="p-c-icon bp-set vm"><img class="lazy" data-original="{$tpl_images_uri}products/icon_banner_ship.png" alt="" /></div>
							<div class="clr"></div>
							<div class="p-c-icon bp-set"><img class="lazy" data-original="{$tpl_images_uri}products/icon_banner_anchor.png" alt="" /></div>
						</div>
					{/if}
					{if $page_name !='index' && $page_name !='pagenotfound'}
						{include file="$tpl_dir./breadcrumb.tpl"}
					{/if}
					{if $page_name !='index'}
					<div class="{if $page_name=='cms' && $cms->id=='4'}aboutus-content {else if $page_name=='product'}product-details {else if $page_name=='category'}pr-co-main animated hiding {else if $page_name=='order'}checkout-content {/if}{if $page_name!='product'}mb5 {/if}pad-edge"{if $page_name=='category'} data-animation="fadeInUp" data-delay="100"{/if}>
						<div class="auto{if $page_name !='category' && $page_name !='product' && $page_name !='search'} small{/if}">
					{/if}
					{*<div id="slider_row" class="row">
						{capture name='displayTopColumn'}{hook h='displayTopColumn'}{/capture}
						{if $smarty.capture.displayTopColumn}
							<div id="top_column" class="center_column col-xs-12 col-sm-12">{$smarty.capture.displayTopColumn}</div>
						{/if}
					</div>
					<div class="row">
						{if isset($left_column_size) && !empty($left_column_size)}
						<div id="left_column" class="column col-xs-12 col-sm-{$left_column_size|intval}">{$HOOK_LEFT_COLUMN}</div>
						{/if}
						{if isset($left_column_size) && isset($right_column_size)}{assign var='cols' value=(12 - $left_column_size - $right_column_size)}{else}{assign var='cols' value=12}{/if}
						<div id="center_column" class="center_column col-xs-12 col-sm-{$cols|intval}">*}
	{/if}
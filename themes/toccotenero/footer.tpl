{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if !isset($content_only) || !$content_only}
					</div><!-- #center_column -->
					{if isset($right_column_size) && !empty($right_column_size)}
						<div id="right_column" class="col-xs-12 col-sm-{$right_column_size|intval} column">{$HOOK_RIGHT_COLUMN}</div>
					{/if}
				{*	</div><!-- .row -->
					</div><!-- #columns -->*}
				{if isset($page_name) && $page_name !='index'}
						</div>
					</div>				
				</div><!-- .inner-content -->
				{/if}
			</section><!-- .columns-container -->
			{if isset($HOOK_FOOTER)}
			<!-- Footer -->
				<footer id="footer">
					<div class="footer-content par-medium">
						<div class="footer-top pad-edge bp-center">
							<div class="auto bp-rel">
								<div class="auto small">
									{$HOOK_FOOTER}
								</div>
							</div>
						</div>
						<div class="footer-bottom pad-edge">
							<div class="auto bp-rel">
								<div class="he-to-social bp-ab bp-middle">
									<ul>
										{if isset($facebook_url) && $facebook_url != ''}
											<li>
												<a target="_blank" class="_blank" href="{$facebook_url|escape:html:'UTF-8'}">
													{*<span>{l s='Facebook' mod='blocksocial'}</span>*}
								                    <i class="fa fa-facebook"></i>
												</a>
											</li>
										{/if}
										{if isset($instagram_url) && $instagram_url != ''}
								        	<li class="instagram">
								        		<a target="_blank" class="_blank" href="{$instagram_url|escape:html:'UTF-8'}">
								        			{*<span>{l s='Instagram' mod='blocksocial'}</span>*}
								                    <i class="fa fa-instagram"></i>
								        		</a>
								        	</li>
								        {/if}
								        {if isset($youtube_url) && $youtube_url != ''}
								            <li class="youtube">
								                <a target="_blank" class="_blank" href="{$youtube_url|escape:html:'UTF-8'}">
								                    {*<span>{l s='Youtube' mod='blocksocial'}</span>*}
								                    <i class="fa fa-youtube-play"></i>
								                </a>
								            </li>
								        {/if}
									</ul>
								</div>
								<div class="fo-logo bp-center"><img class="lazy" src="{$tpl_images_uri}page_template/logo_t.png" data-original="{$tpl_images_uri}page_template/logo_t.png" width="35" height="49" alt="" title="" /></div>
								<div class="fo-copyright bp-ab bp-middle bp-tt"><i class="fa fa-copyright"></i> Tenero {'Y'|date}. {$footer_text}</div>
							</div>
						</div>
					</div>
				</footer>
			<!-- #footer -->
			{/if}
		{*}</div><!-- #page -->{*}
{/if}
</section>
{include file="$tpl_dir./global.tpl"}
	</body>
</html>
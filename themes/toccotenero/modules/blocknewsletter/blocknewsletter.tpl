{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!-- Block Newsletter module-->
{strip}
{addJsDef msg_newsl=''}
{/strip}

<script type="text/javascript">
{literal}
$(document).ready(function(){
	$("#submitNewsletter").click(function(event) {
		event.preventDefault();
		var myemail = $('#newsletter-input').val();
			response = 'Invalid email address.';
		if(myemail) {
			$.get('modules/blocknewsletter/blocknewsletter-ajax.php?email='+myemail, 
				function(response) {
				if(response !='Invalid email address.'){	
					if(response=='subscribed') {
						msg_color = '#35B33F';
						response = 'You have successfully subscribed to this newsletter.';
					}
					else
						msg_color = 'red';
				}
				else
				{
					msg_color = 'red';
				}			

		$('#msg_note').html(response);
		$('#msg_note').css('color',msg_color);	
			});
		}
		else{
			msg_color = 'red';
			$('#msg_note').html(response);
			$('#msg_note').css('color',msg_color);
		}
	});
});
{/literal}
</script>
<div class="fo-to-set bp-set vt bp-left fo-to-newsletter">
	<h4>{l s='Always be the first to know!' mod='blocknewsletter'}</h4>
	<div class="msg_note" id="msg_note" style="font-size:18px;"></div>
	{if isset($msg) && $msg}<div class="msg_note" style="color:{if $nw_error}red{else}#35B33F{/if}; font-size:18px;">{$msg}</div>{/if}
	<div class="f-t-new-form">
		<form action="#newsletter-input"{*action="{$link->getPageLink('index', null, null, null, false, null, true)|escape:'html':'UTF-8'}"*} method="post">
			
			<div class="{if isset($msg) && $msg } {if $nw_error}form-error{else}form-ok{/if}{/if}" >
				<input class="fl inputNew newsletter-input" id="newsletter-input" type="email" name="email" size="18" value="{if isset($value) && $value}{$value}{else}{l s='' mod='blocknewsletter'}{/if}" placeholder="your email.."/>
                <button type="submit" name="submitNewsletter" class="fl" id="submitNewsletter">
                    <i class="fa fa-angle-right"></i>
                </button>
				<input type="hidden" name="action" value="0" />
			</div>
			<div class="clr"></div>
		</form>
	</div>
    {hook h="displayBlockNewsletterBottom" from='blocknewsletter'}
</div>
<!-- /Block Newsletter module-->
{strip}
{if isset($msg) && $msg}
{addJsDef msg_newsl=$msg|@addcslashes:'\''}
{/if}
{if isset($nw_error)}
{addJsDef nw_error=$nw_error}
{/if}
{addJsDefL name=placeholder_blocknewsletter}{l s='Enter your e-mail' mod='blocknewsletter' js=1}{/addJsDefL}
{if isset($msg) && $msg}
	{addJsDefL name=alert_blocknewsletter}{l s='Newsletter : %1$s' sprintf=$msg js=1 mod="blocknewsletter"}{/addJsDefL}
{/if}
{/strip}
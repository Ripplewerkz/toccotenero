function mr_parallax() {
    "use strict";

    function e(e) {
        for (var t = 0; t < e.length; t++)
            if ("undefined" != typeof document.body.style[e[t]]) return e[t];
        return null
    }

    function t() {
        var e, t = 0;
        return s() ? (t = jQuery("header").find(".header-content").outerHeight(!0), e = jQuery("header").find(".header-content").css("position"), ("absolute" === e || "fixed" === e) && (t = 0)) : t = jQuery(document).find("nav:first").outerHeight(!0), t
    }

    function n(e, t, n, o) {
        var r = e - 1;
        return r /= o, e /= o, r--, e--, n * (e * e * e * e * e + 1) + t - (n * (r * r * r * r * r + 1) + t)
    }

    function o() {
        if (_) {
            for (var e = u.length, t = a(); e--;) r(u[e], t, h, w);
            _ = !1
        }
        y && (b += -T * n(x, 0, M, P), (b > Q || -Q > b) && (S.scrollBy(0, b), b = 0), x++, x > P && (x = 0, y = !1, v = !0, T = 0, E = 0, H = 0, b = 0)), m(o)
    }

    function r(e, t, n, o) {
        var r = s();
        r ? t + p - g > e.elemTop && t - g < e.elemBottom && (e.isFirstSection ? e.imageHolder.style[f] = n + t / 2 + o : e.imageHolder.style[f] = n + (t - e.elemTop - g) / 2 + o) : t + p > e.elemTop && t < e.elemBottom && (e.isFirstSection ? e.imageHolder.style[f] = n + t / 2 + o : e.imageHolder.style[f] = n + (t + p - e.elemTop) / 2 + o)
    }

    function a() {
        return S != window ? S.scrollTop : 0 === document.documentElement.scrollTop ? document.body.scrollTop : document.documentElement.scrollTop
    }

    function i() {
        _ = !0
    }

    function l(e) {
        e.preventDefault && e.preventDefault(), T = e.notRealWheel ? -e.deltaY / 4 : 1 == e.deltaMode ? -e.deltaY / 3 : 100 === Math.abs(e.deltaY) ? -e.deltaY / 120 : -e.deltaY / 40, T = -j > T ? -j : T, T = T > j ? j : T, y = !0, x = D
    }

    function d(e) {
        var t = {};
        return e && "[object Function]" === t.toString.call(e)
    }

    function s() {
        return "undefined" == typeof window.mr_variant ? !1 : !0
    }
    var u, m = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame,
        c = ["transform", "msTransform", "webkitTransform", "mozTransform", "oTransform"],
        f = e(c),
        h = "translate3d(0,",
        w = "px,0)",
        p = Math.max(document.documentElement.clientHeight, window.innerHeight || 0),
        g = 0,
        y = !1,
        v = !0,
        x = 0,
        T = 0,
        E = 0,
        H = 0,
        j = 2,
        D = 4,
        M = 300,
        Q = 1,
        P = 30,
        b = 0,
        S = window,
        _ = (s(), !1),
        L = this;
    jQuery(document).ready(function() {
        L.documentReady()
    }), jQuery(window).load(function() {
        L.windowLoad()
    }), this.getScrollingState = function() {
        return x > 0 ? !0 : !1
    }, this.documentReady = function(e) {
        return p = Math.max(document.documentElement.clientHeight, window.innerHeight || 0), jQuery("body").hasClass("parallax-2d") && (h = "translate(0,", w = "px)"), /Android|iPad|iPhone|iPod|BlackBerry|Windows Phone/i.test(navigator.userAgent || navigator.vendor || window.opera) ? jQuery(".parallax").removeClass("parallax") : m && (L.profileParallaxElements(), L.setupParallax()), d(e) ? void e() : void 0
    }, this.windowLoad = function() {
        p = Math.max(document.documentElement.clientHeight, window.innerHeight || 0), g = t(), window.mr_parallax.profileParallaxElements()
    }, this.setupParallax = function() {
        s() && (S = jQuery(".viu").get(0), "undefined" != typeof S && (S.scrollBy = function(e, t) {
            this.scrollTop += t, this.scrollLeft += e
        })), "undefined" != typeof S && (S.addEventListener("scroll", i, !1), window.addWheelListener(S, l, !1), window.addEventListener("resize", function() {
            p = Math.max(document.documentElement.clientHeight, window.innerHeight || 0), g = t(), L.profileParallaxElements()
        }, !1), o())
    }, this.profileParallaxElements = function() {
        u = [], g = t();
        var e = s(),
            n = ".parallax .background-image-holder, .parallax ul.slides li .background-image-holder";
        e && (n = ".parallax .background-image-holder, .parallax ul.slides li .background-image-holder"), jQuery(n).each(function(t) {
            var n = jQuery(this).closest(".parallax"),
                o = e ? n.position().top : n.offset().top;
            u.push({
                section: n.get(0),
                outerHeight: n.outerHeight(),
                elemTop: o,
                elemBottom: o + n.outerHeight(),
                isFirstSection: n.is(":nth-of-type(1)") ? !0 : !1,
                imageHolder: jQuery(this).get(0)
            }), e ? e && (n.is(":nth-of-type(1)") ? L.mr_setTranslate3DTransform(jQuery(this).get(0), 0 === a() ? 0 : a() / 2) : L.mr_setTranslate3DTransform(jQuery(this).get(0), (a() - o - g) / 2)) : n.is(":nth-of-type(1)") ? L.mr_setTranslate3DTransform(jQuery(this).get(0), 0 === a() ? 0 : a() / 2) : L.mr_setTranslate3DTransform(jQuery(this).get(0), (a() + p - o) / 2)
        })
    }, this.mr_setTranslate3DTransform = function(e, t) {
        e.style[f] = h + t + w
    }
}
window.mr_parallax = new mr_parallax,
    function(e, t) {
        function n(t, n, i, l) {
            t[o](a + n, "wheel" == r ? i : function(t) {
                !t && (t = e.event);
                var n = {
                    originalEvent: t,
                    target: t.target || t.srcElement,
                    type: "wheel",
                    deltaMode: "MozMousePixelScroll" == t.type ? 0 : 1,
                    deltaX: 0,
                    deltaZ: 0,
                    notRealWheel: 1,
                    preventDefault: function() {
                        t.preventDefault ? t.preventDefault() : t.returnValue = !1
                    }
                };
                return "mousewheel" == r ? (n.deltaY = -1 / 40 * t.wheelDelta, t.wheelDeltaX && (n.deltaX = -1 / 40 * t.wheelDeltaX)) : n.deltaY = t.detail / 3, i(n)
            }, l || !1)
        }
        var o, r, a = "";
        e.addEventListener ? o = "addEventListener" : (o = "attachEvent", a = "on"), r = "onwheel" in t.createElement("div") ? "wheel" : "undefined" != typeof t.onmousewheel ? "mousewheel" : "DOMMouseScroll", e.addWheelListener = function(e, t, o) {
            n(e, r, t, o), "DOMMouseScroll" == r && n(e, "MozMousePixelScroll", t, o)
        }
    }(window, document);

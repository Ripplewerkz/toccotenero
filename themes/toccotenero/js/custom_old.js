function resize() {
	var windowWidth = $(window).width();
	var windowHeight = $(window).height();

	// STICKY FOOTER
	var headerHeight = $('header').outerHeight();
	var footerHeight = $('footer').outerHeight();
	var footerTop = (footerHeight) * -1
	$('footer').css({marginTop: footerTop});
	$('#main-wrapper').css({paddingTop: headerHeight, paddingBottom: footerHeight});

	// for vertically middle content
	$('.bp-middle').each(function() {
		var bpMiddleHeight = $(this).outerHeight() / 2 * - 1;
		$(this).css({marginTop: bpMiddleHeight});
	});

	// for full screen content
	var topHeight = windowHeight - $('header').outerHeight();
	$('.bp-height').css({height: windowHeight});
	$('.bp-topheight').css({height: topHeight});

	// for height balancing of the content
	var bpHeight = 0;
	$('.bp-set-height').each(function() {
		if($(this).height() > bpHeight){
			bpHeight = $(this).height();
		}
	});
	$('.bp-set-height').css({height: bpHeight});
}

$(window).scroll(function() {
	bpOnScroll();
});

$(window).resize(function() {
	resize();
});

$(document).ready(function() {
	// IMAGE LAZY LOAD
	$("img.lazy").lazyload();

	// INVIEW
	if (!Modernizr.touch) {
		$.each($('.animated'),function(){
			$(this).on('inview',function(event,visible){
				if (visible == true) {
					var element = $(this);
					var animation = element.data('animation');
					var animationDelay = element.data('delay');
					if(animationDelay) {
						setTimeout(function(){
							element.addClass(animation + ' visible');
							element.removeClass('hiding');
						}, animationDelay);
					} else {
						element.addClass(animation + ' visible');
						element.removeClass('hiding');
					}
				}
			});
		});
	} else {
		$('.animated').removeClass('animated hiding');
	}

	// PARALLAX BACKGROUND
	$('.background-image-holder').each(function() {
        var imgSrc = $(this).children('img').attr('src');
        $(this).css('background-image', 'url("' + imgSrc + '")');
        $(this).children('img').hide();
        $(this).css('background-position', 'initial');
    });
	// Fade in background images
	setTimeout(function() {
		$('.background-image-holder').each(function() {
			$(this).addClass('fadeIn');
		});
	}, 200);
	// Disable parallax on mobile
	if (Modernizr.touch) {
		$('section').removeClass('parallax');
		$('.background-image-holder').addClass('mobile');
	}

	// AUTO SCROLL
	$('.bp-autoscroll').click(function(event) {
		var layer = $(this).attr('data-scroll');
		$('body, html').animate({scrollTop:$(layer).offset().top}, 1000);
		event.preventDefault();
	});

	// HOME SLIDER
	$('.home-banner .flexslider').flexslider({
    animation: "fade",
		directionNav: true,
		controlNav: false,
		prevText: '',
		nextText: '',
		start: function(slider) {
			$('.home-banner .flex-direction-nav a').click(function(event){
				$('.home-banner .flexslider').flexslider("play");
			});
		}
  });


	// for product details slider
	$('.p-d-slider #carousel').flexslider({
    animation: "slide",
		direction: 'vertical',
		directionNav: false,
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: 174,
    itemMargin: 6,
    asNavFor: '.p-d-slider #slider'
  });

  $('.p-d-slider #slider').flexslider({
    animation: "slide",
		directionNav: false,
    controlNav: true,
    animationLoop: false,
    slideshow: false,
    sync: ".p-d-slider #carousel",
		touch: false
  });

	// for hoverdir
	$('.hoverdir-content .item').each(function() {
		$(this).hoverdir();
	});

	// for sliding menu
	$('.menu-bar').click(function() {
		$(this).addClass('active');
		$('.menu-sliding').addClass('active');
		$('.menu-sliding.cover').fadeIn(200);
	});
	$('.menu-sliding.cover').click(function() {
		$('.menu-bar').removeClass('active');
		$('.menu-sliding').removeClass('active');
		$('.menu-sliding.cover').fadeOut(200);
	});
	$('.me-sl-menu li .sub').click(function() {
		if($(this).hasClass('active')) {
			$(this).removeClass('active');
			$(this).next().slideUp(200);
		} else {
			$('.m-s-menu-sub').prev().removeClass('active');
			$('.m-s-menu-sub').slideUp(200);
			$(this).addClass('active');
			$(this).next().slideDown(200);
		}
	});

	// for toggle content
	$('.bp-dd-title').first().addClass('active');
	$('.bp-dd-title').first().next().slideDown(200);
	$('.bp-dd-title').click(function() {
		$(this).toggleClass('active');
		$(this).next().slideToggle(200);
	});

	// CUSTOM SELECT
	$('select.custom-select').each(function() {
		$(this).wrap('<div class="custom-select-wrapper" />');
		$(this).before('<div class="custom-select-display" />');
		$(this).change(function() {
			$(this).siblings('.custom-select-display').text( $(this).find('option:selected').text() );
		});
		$(this).keyup(function() {
			$(this).siblings('.custom-select-display').text( $(this).find('option:selected').text() );
		});
		$(this).change();
	});

	bpOnScroll();
	resize();
});

$(window).load(function() {
	resize();
});

// preloader once done
Pace.on('done', function() {
	// totally hide the preloader especially for IE
	setTimeout(function() {
		$('.pace-inactive').hide();
	}, 500);
});

function bpOnScroll() {
	// for sticky header
	if (!Modernizr.touch) {
		var headerHeight = $('.header-content').outerHeight();
		if($(window).scrollTop() > headerHeight ){
			$('header').addClass('v2');
			$('.header-top').slideUp(200);
			$('.menu-bar, header .logo').slideUp(200);
			$('.menu,.logo-left').slideDown(200);
		} else {
			$('header').removeClass('v2');
			$('.header-top').slideDown(200);
			$('.menu-bar, header .logo').slideDown(200);
			$('.menu,.logo-left').slideUp(200);
			setTimeout(function() {
				//resize();
			}, 200);
		}
	}
}

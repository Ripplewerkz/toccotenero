function resize() {

	var windowWidth = $(window).width();

	var windowHeight = $(window).height();



	// STICKY FOOTER

	var headerHeight = $('header').outerHeight();

	var footerHeight = $('footer').outerHeight();

	var footerTop = (footerHeight) * -1

	$('footer').css({marginTop: footerTop});

	$('#main-wrapper').css({paddingTop: headerHeight, paddingBottom: footerHeight});



	// for vertically middle content

	$('.bp-middle').each(function() {

		var bpMiddleHeight = $(this).outerHeight() / 2 * - 1;

		$(this).css({marginTop: bpMiddleHeight});

	});



	// for full screen content

	// var topHeight = windowHeight - $('header').outerHeight();

	// $('.bp-height').css({height: windowHeight});

	// $('.bp-topheight').css({height: topHeight});



	// for height balancing of the content

	var bpHeight = 0;

	$('.bp-set-height').each(function() {

		if($(this).height() > bpHeight){

			bpHeight = $(this).height();

		}

	});

	$('.bp-set-height').css({height: bpHeight});

}



$(window).resize(function() {

	resize();

});

$(document).ready(function() {

	if (Modernizr.touch) {

		$('html').addClass('bp-touch');

	}



	// IMAGE LAZY LOAD

	$("img.lazy").lazyload();


	// for product details accessories slider

	$('.product-details #carousel_h').flexslider({

    animation			: "slide",

		direction			: 'horizontal',

		directionNav	: true,

    controlNav		: false,

    animationLoop	: true,

    slideshow			: true,

    itemWidth			: 250,

    itemMargin		: 6,

		prevText			: '',

		nextText			: '',

	minItems: 1, // use function to pull in initial value
    maxItems: 5

  });

	// INVIEW

	if (!Modernizr.touch) {

		$.each($('.animated'),function(){

			$(this).on('inview',function(event,visible){

				if (visible == true) {

					var element = $(this);

					var animation = element.data('animation');

					var animationDelay = element.data('delay');

					if(animationDelay) {

						setTimeout(function(){

							element.addClass(animation + ' visible');

							element.removeClass('hiding');

						}, animationDelay);

					} else {

						element.addClass(animation + ' visible');

						element.removeClass('hiding');

					}

				}

			});

		});

	} else {

		$('.animated').removeClass('animated hiding');

	}



	// PARALLAX BACKGROUND

	$('.background-image-holder').each(function() {

        // var imgSrc = $(this).children('img').attr('src');

        // $(this).css('background-image', 'url("' + imgSrc + '")');

        // $(this).children('img').hide();

        // $(this).css('background-position', 'initial');

    });

	// Fade in background images

	setTimeout(function() {

		$('.background-image-holder').each(function() {

			$(this).addClass('fadeIn');

		});

	}, 200);

	// Disable parallax on mobile

	if (Modernizr.touch) {

		$('section').removeClass('parallax');

		$('.background-image-holder').addClass('mobile');

	}



	// AUTO SCROLL

	$('.bp-autoscroll').click(function(e) {

		var layer = $(this).attr('data-scroll');

		$('body, html').animate({scrollTop:$(layer).offset().top}, 1000);

		event.preventDefault();

	});



	// HOME SLIDER

	$('.home-banner .flexslider').flexslider({

    animation: "fade",

		directionNav: true,

		controlNav: false,

		prevText: '',

		nextText: '',

		start: function(slider) {

			$('.home-banner .flex-direction-nav a').click(function(event){

				$('.home-banner .flexslider').flexslider("play");

			});

		}

  });





	// for product details slider

	$('.p-d-slider #carousel').flexslider({

    animation			: "slide",

		direction			: 'vertical',

		directionNav	: true,

    controlNav		: false,

    animationLoop	: false,

    slideshow			: false,

    itemWidth			: 174,

    itemMargin		: 6,

		prevText			: '',

		nextText			: '',

    asNavFor			: '.p-d-slider #slider'

  });

	


  $('.p-d-slider #slider').flexslider({

    animation: "slide",

		directionNav: false,

    controlNav: true,

    animationLoop: false,

    slideshow: false,

    sync: ".p-d-slider #carousel",

		touch: false

  });



	// for lookbook

	$('.lookbook-banner .flexslider').flexslider({

    animation			: "fade",

		directionNav	: true,

		controlNav		: true,

		prevText			: '',

		nextText			: '',

		loop					: false,

		start: function(slider) {

			// autoplay the slide

			$('.lookbook-banner .flex-direction-nav a').click(function(event){

				$('.lookbook-banner .flexslider').flexslider("play");

			});



			// for bullet vertical position

			var lobaBulletHeight = ($('.lookbook-banner .flex-control-paging').outerHeight() / 2) * -1;

			$('.lookbook-banner .flex-control-paging').css({marginTop: lobaBulletHeight});



			// for customize navigation

			$('.lo-ba-nav.left').click(function() {

				$('.lookbook-banner .flexslider').flexslider("prev");

				$('.lookbook-banner .flexslider').flexslider("play");

			});

			$('.lo-ba-nav.right').click(function() {

				$('.lookbook-banner .flexslider').flexslider("next");

				$('.lookbook-banner .flexslider').flexslider("play");

			});



			var currentSlide = slider.currentSlide;

			var allSlide = slider.count;

			prevNum = 0;

			nextNum = 0;

			if (currentSlide == 0) {

				prevNum = slider.count;

				nextNum = parseInt(slider.currentSlide + 2);

			} else if (parseInt(slider.currentSlide + 1) == slider.count) {

				prevNum = parseInt(slider.currentSlide);

				nextNum = 1;

			} else {

				prevNum = parseInt(slider.currentSlide);

				nextNum	= parseInt(slider.currentSlide + 2)

			}

			var prevSlideText = $('.lookbook-banner ul.slides li:nth-child(' + prevNum  + ') .ho-ba-tooltip').html();

			$('.lookbook-banner .flex-prev').html(prevSlideText);

			var nextSlideText = $('.lookbook-banner ul.slides li:nth-child(' + nextNum + ') .ho-ba-tooltip').html();

			$('.lookbook-banner .flex-next').html(nextSlideText);



			$('.lookbook-banner .flex-direction-nav a').addClass('bp-middle');

			resize();

		},

		after: function(slider) {

			var currentSlide = slider.currentSlide;

			var allSlide = slider.count;

			prevNum = 0;

			nextNum = 0;

			if (currentSlide == 0) {

				prevNum = slider.count;

				nextNum = parseInt(slider.currentSlide + 2);

			} else if (parseInt(slider.currentSlide + 1) == slider.count) {

				prevNum = parseInt(slider.currentSlide);

				nextNum = 1;

			} else {

				prevNum = parseInt(slider.currentSlide);

				nextNum	= parseInt(slider.currentSlide + 2)

			}

			var prevSlideText = $('.lookbook-banner ul.slides li:nth-child(' + prevNum  + ') .ho-ba-tooltip').html();

			$('.lookbook-banner .flex-prev').html(prevSlideText);

			var nextSlideText = $('.lookbook-banner ul.slides li:nth-child(' + nextNum + ') .ho-ba-tooltip').html();

			$('.lookbook-banner .flex-next').html(nextSlideText);



			$('.lookbook-banner .flex-direction-nav a').addClass('bp-middle');

			resize();

		}

  });







	// for hoverdir

	$('.hoverdir-content .item').each(function() {

		$(this).hoverdir();

	});



	// for sliding menu

	$('.menu-bar').click(function() {

		$(this).addClass('active');

		$('.menu-sliding').addClass('active');

		$('.menu-sliding.cover').fadeIn(200);

		$('html').addClass('menu-active');

	});

	$('.me-sl-close').click(function() {

		$('html').removeClass('menu-active');

		$('.menu-bar').removeClass('active');

		$('.menu-sliding').removeClass('active');

		$('.menu-sliding.cover').fadeOut(200);

	});



	// for toggle content

	$('.bp-dd-title').first().addClass('active');

	$('.bp-dd-title').first().next().slideDown(200);

	$('.bp-dd-title').click(function() {

		$(this).toggleClass('active');

		$(this).next().slideToggle(200);

	});



	// CUSTOM SELECT

	$('select.custom-select').each(function() {

		$(this).wrap('<div class="custom-select-wrapper" />');

		$(this).before('<div class="custom-select-display" />');

		$(this).change(function() {

			$(this).siblings('.custom-select-display').text( $(this).find('option:selected').text() );

		});

		$(this).keyup(function() {

			$(this).siblings('.custom-select-display').text( $(this).find('option:selected').text() );

		});

		$(this).change();

	});



	resize();

});



$(window).load(function() {

	resize();

});



// preloader once done

Pace.on('done', function() {

	// totally hide the preloader especially for IE

	setTimeout(function() {

		$('.pace-inactive').hide();

	}, 500);

});


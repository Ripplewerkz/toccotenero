{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<div class="home-content">
	{banners_get_banners('Homepage Slider' , 'homepage_slider.tpl')}
	
	{if isset($HOOK_HOME) && $HOOK_HOME|trim}
		<div class="home-products bp-rel animated hiding" data-animation="fadeInUp" data-delay="100">
			{$HOOK_HOME}	
	{/if}
			{capture name='displayHomeProducts'}{hook h='displayHomeProducts'}{/capture}
			{if $smarty.capture.displayHomeProducts}
			<div class="home-popular bp-center pad-edge animated hiding" data-animation="fadeInUp" data-delay="100">
				<div class="auto">
					<h1 class="ho-co-title">{l s='most popular'}</h1>
					{$smarty.capture.displayHomeProducts}
				</div>
			</div>
			{/if}
			{banners_get_banners('Home Instagram' , 'homepage_instagram.tpl')}
		</div>
</div>
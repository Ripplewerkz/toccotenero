<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class Meta extends MetaCore
{

/**
     * Get meta tags for a given page
     *
     * @since 1.5.0
     * @param int $id_lang
     * @param string $page_name
     * @return array Meta tags
     */
    public static function getHomeMetas($id_lang, $page_name)
    {
        $metas = Meta::getMetaByPage($page_name, $id_lang);
        $ret['meta_title'] = (isset($metas['title']) && $metas['title']) ? Configuration::get('PS_SHOP_NAME').' | '.$metas['title'] : Configuration::get('PS_SHOP_NAME');
        $ret['meta_description'] = (isset($metas['description']) && $metas['description']) ? $metas['description'] : '';
        $ret['meta_keywords'] = (isset($metas['keywords']) && $metas['keywords']) ? $metas['keywords'] :  '';
        return $ret;
    }

    /**
     * Get CMS meta tags
     *
     * @since 1.5.0
     * @param int $id_cms
     * @param int $id_lang
     * @param string $page_name
     * @return array
     */
    public static function getCmsMetas($id_cms, $id_lang, $page_name)
    {
        $sql = 'SELECT `meta_title`, `meta_description`, `meta_keywords`
                FROM `'._DB_PREFIX_.'cms_lang`
                WHERE id_lang = '.(int)$id_lang.'
                    AND id_cms = '.(int)$id_cms.
                    ((int)Context::getContext()->shop->id ?
                        ' AND id_shop = '.(int)Context::getContext()->shop->id : '');

        if ($row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql)) {
            $row['meta_title'] = Configuration::get('PS_SHOP_NAME').' | '.$row['meta_title'];
            return Meta::completeMetaTags($row, $row['meta_title']);
        }

        return Meta::getHomeMetas($id_lang, $page_name);
    }

    /**
     * Get category meta tags
     *
     * @since 1.5.0
     * @param int $id_category
     * @param int $id_lang
     * @param string $page_name
     * @return array
     */
    public static function getCategoryMetas($id_category, $id_lang, $page_name, $title = '')
    {
        if (!empty($title)) {
            $title = ' - '.$title;
        }
        $page_number = (int)Tools::getValue('p');
        $sql = 'SELECT `name`, `meta_title`, `meta_description`, `meta_keywords`, `description`
                FROM `'._DB_PREFIX_.'category_lang` cl
                WHERE cl.`id_lang` = '.(int)$id_lang.'
                    AND cl.`id_category` = '.(int)$id_category.Shop::addSqlRestrictionOnLang('cl');

        $cache_id = 'Meta::getCategoryMetas'.(int)$id_category.'-'.(int)$id_lang;
        if (!Cache::isStored($cache_id)) {
            if ($row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql)) {
                if (empty($row['meta_description'])) {
                    $row['meta_description'] = strip_tags($row['description']);
                }

                // Paginate title
                if (!empty($row['meta_title'])) {
                    $row['meta_title'] = Configuration::get('PS_SHOP_NAME').' | '.$title.$row['meta_title'].(!empty($page_number) ? ' ('.$page_number.')' : '');
                } else {
                    $row['meta_title'] = Configuration::get('PS_SHOP_NAME').' | '.$row['name'].(!empty($page_number) ? ' ('.$page_number.')' : '');
                }

                if (!empty($title)) {
                    $row['meta_title'] = Configuration::get('PS_SHOP_NAME').' | '.$title.(!empty($page_number) ? ' ('.$page_number.')' : '');
                }

                $result = Meta::completeMetaTags($row, $row['name']);
            } else {
                $result = Meta::getHomeMetas($id_lang, $page_name);
            }
            Cache::store($cache_id, $result);
            return $result;
        }
        return Cache::retrieve($cache_id);
    }

    /**
     * Get supplier meta tags
     *
     * @since 1.5.0
     * @param int $id_supplier
     * @param int $id_lang
     * @param string $page_name
     * @return array
     */
    public static function getSupplierMetas($id_supplier, $id_lang, $page_name)
    {
        $sql = 'SELECT `name`, `meta_title`, `meta_description`, `meta_keywords`
                FROM `'._DB_PREFIX_.'supplier_lang` sl
                LEFT JOIN `'._DB_PREFIX_.'supplier` s ON (sl.`id_supplier` = s.`id_supplier`)
                WHERE sl.id_lang = '.(int)$id_lang.'
                    AND sl.id_supplier = '.(int)$id_supplier;
        if ($row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql)) {
            if (!empty($row['meta_description'])) {
                $row['meta_description'] = strip_tags($row['meta_description']);
            }
            if (!empty($row['meta_title'])) {
                $row['meta_title'] = Configuration::get('PS_SHOP_NAME').' | '.$row['meta_title'];
            }
            return Meta::completeMetaTags($row, $row['name']);
        }

        return Meta::getHomeMetas($id_lang, $page_name);
    }

    /**
     * Get CMS category meta tags
     *
     * @since 1.5.0
     * @param int $id_cms_category
     * @param int $id_lang
     * @param string $page_name
     * @return array
     */
    public static function getCmsCategoryMetas($id_cms_category, $id_lang, $page_name)
    {
        $sql = 'SELECT `meta_title`, `meta_description`, `meta_keywords`
                FROM `'._DB_PREFIX_.'cms_category_lang`
                WHERE id_lang = '.(int)$id_lang.'
                    AND id_cms_category = '.(int)$id_cms_category.
                    ((int)Context::getContext()->shop->id ?
                        ' AND id_shop = '.(int)Context::getContext()->shop->id : '');
        if ($row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql)) {
            $row['meta_title'] = Configuration::get('PS_SHOP_NAME').' | '.$row['meta_title'];
            return Meta::completeMetaTags($row, $row['meta_title']);
        }

        return Meta::getHomeMetas($id_lang, $page_name);
    }

    /**
     * Get manufacturer meta tags
     *
     * @since 1.5.0
     * @param int $id_manufacturer
     * @param int $id_lang
     * @param string $page_name
     * @return array
     */
    public static function getManufacturerMetas($id_manufacturer, $id_lang, $page_name)
    {
        $page_number = (int)Tools::getValue('p');
        $sql = 'SELECT `name`, `meta_title`, `meta_description`, `meta_keywords`
                FROM `'._DB_PREFIX_.'manufacturer_lang` ml
                LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (ml.`id_manufacturer` = m.`id_manufacturer`)
                WHERE ml.id_lang = '.(int)$id_lang.'
                    AND ml.id_manufacturer = '.(int)$id_manufacturer;
        if ($row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql)) {
            if (!empty($row['meta_description'])) {
                $row['meta_description'] = strip_tags($row['meta_description']);
            }

            $row['meta_title'] = Configuration::get('PS_SHOP_NAME').' | ';
            $row['meta_title'] .= ($row['meta_title'] ? $row['meta_title'] : $row['name']).(!empty($page_number) ? ' ('.$page_number.')' : '');
            return Meta::completeMetaTags($row, $row['meta_title']);
        }

        return Meta::getHomeMetas($id_lang, $page_name);
    }

    /**
     * @since 1.5.0
     */
    public static function completeMetaTags($meta_tags, $default_value, Context $context = null)
    {
        if (!$context) {
            $context = Context::getContext();
        }

        if (empty($meta_tags['meta_title'])) {
            $meta_tags['meta_title'] = Configuration::get('PS_SHOP_NAME').' | '.$default_value;
        }
        if (empty($meta_tags['meta_description'])) {
            $meta_tags['meta_description'] = Configuration::get('PS_META_DESCRIPTION', $context->language->id) ? Configuration::get('PS_META_DESCRIPTION', $context->language->id) : '';
        }
        if (empty($meta_tags['meta_keywords'])) {
            $meta_tags['meta_keywords'] = Configuration::get('PS_META_KEYWORDS', $context->language->id) ? Configuration::get('PS_META_KEYWORDS', $context->language->id) : '';
        }
        return $meta_tags;
    }
}